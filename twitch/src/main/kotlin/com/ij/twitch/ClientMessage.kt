package com.ij.twitch

/**
 * Created by ilben on 5.4.2017.
 */
sealed class ClientMessage {
    class Pass(val accessToken: String) : ClientMessage()
    class Nick(val nick: String) : ClientMessage()
    class Join(val channel: String) : ClientMessage()
    class Part(val channel: String) : ClientMessage()
    class CapReq(val capability: Capability) : ClientMessage()
    class Pong : ClientMessage()
}

enum class Capability {
    Membership,
    Tags,
    Commands
}