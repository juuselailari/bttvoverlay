package com.ij.twitch

/**
 * Created by ilben on 5.4.2017.
 */
sealed class ServerMessage(val line: String) {
    class Invalid(line: String) : ServerMessage(line)
    class Valid(line: String, val message: Message) : ServerMessage(line)
}

data class Message(
        val tags: HashMap<String, String>,
        val prefix: String?,
        val command: String?,
        val params: List<String>) {

    companion object {

        private val TAG_DELIMITER_REGEX = ";".toRegex()
        private val KEY_VALUE_REGEX = "=".toRegex()

        fun parse(line: String): Message? {

            val tags = hashMapOf<String, String>()
            var prefix: String? = null
            var command: String? = null

            var position = 0
            var nextspace = 0
            // parsing!
            if (line[0] == '@') {

                nextspace = line.indexOf(" ")
                if (nextspace == -1) {
                    return null
                }

                val rawTags = line.substring(1, nextspace)
                        .split(TAG_DELIMITER_REGEX)
                        .dropLastWhile { it.isEmpty() }

                rawTags.map { tag -> tag.split(KEY_VALUE_REGEX).dropLastWhile { it.isEmpty() } }
                        .forEach {
                            if (it.size == 2) {
                                tags.put(it[0], it[1])
                            } else {
                                tags.put(it[0], "")
                            }
                        }
                position = nextspace + 1
            }

            while (line[position] == ' ') {
                position++
            }

            if (line[position] == ':') {
                nextspace = line.indexOf(" ", position)
                if (nextspace == -1) {
                    return null
                }
                prefix = line.substring(position + 1, nextspace)
                position = nextspace + 1

                while (line[position] == ' ') {
                    position++
                }
            }

            nextspace = line.indexOf(" ", position)

            if (nextspace == -1) {
                if (line.length > position) {
                    command = line.substring(position)
                }
                return Message(tags, prefix, command, emptyList<String>())
            }

            val params = ArrayList<String>()
            command = line.substring(position, nextspace)

            position = nextspace + 1

            while (line[position] == ' ') {
                position++
            }

            while (position < line.length) {
                nextspace = line.indexOf(" ", position)

                if (line[position] == ':') {
                    val param = line.substring(position + 1)
                    params.add(param)
                    break
                }

                if (nextspace != -1) {
                    val param = line.substring(position, nextspace)
                    params.add(param)
                    position = nextspace + 1

                    while (line[position] == ' ') {
                        position++
                    }
                    continue
                }

                if (nextspace == -1) {
                    val param = line.substring(position)
                    params.add(param)
                    break
                }
            }

            return Message(tags, prefix, command, params)
        }
    }

}