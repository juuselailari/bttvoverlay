package com.ij.twitch

import okio.BufferedSink
import okio.BufferedSource

/**
 * Created by ilben on 5.4.2017.
 */

class MessageWriter(private val output: BufferedSink) {

    companion object {
        private val CHARSET = Charsets.UTF_8
    }

    fun writeMessages(messages: Array<ClientMessage>) {
        messages.forEach {
            val exhaust = when (it) {
                is ClientMessage.Pass -> {
                    output.writeString("PASS oauth:${it.accessToken}\r\n", CHARSET)
                }
                is ClientMessage.Nick -> {
                    output.writeString("NICK ${it.nick}\r\n", CHARSET)
                }
                is ClientMessage.Join -> {
                    output.writeString("JOIN #${it.channel}\r\n", CHARSET)
                }
                is ClientMessage.Part -> {
                    output.writeString("PART #${it.channel}\r\n", CHARSET)
                }
                is ClientMessage.Pong -> {
                    output.writeString("PONG :tmi.twitch.tv\r\n", CHARSET)
                }
                is ClientMessage.CapReq -> {
                    val cap = when (it.capability) {
                        Capability.Membership -> "membership"
                        Capability.Tags -> "tags"
                        Capability.Commands -> "commands"
                    }
                    output.writeString("CAP REQ :twitch.tv/$cap\r\n", CHARSET)
                }
            }
        }

        output.flush()
    }

    fun close() {
        output.close()
    }
}

class MessageReader(val input: BufferedSource) {

    var closed = false
        private set

    fun readMessage(): ServerMessage? {
        val line = input.readUtf8Line() ?: return null
        val message = Message.parse(line)
        return if (message == null) ServerMessage.Invalid(line) else ServerMessage.Valid(line, message)
    }

    fun close() {
        closed = true
        input.close()
    }
}