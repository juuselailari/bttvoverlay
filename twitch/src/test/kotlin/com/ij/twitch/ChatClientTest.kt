package com.ij.twitch

import okio.Okio
import org.junit.Test
import java.net.Socket
import java.nio.charset.Charset
import java.util.logging.Logger
import kotlin.test.assertEquals


/**
 * Created by ilben on 5.4.2017.
 */
class ChatClientTest {

    val logger = Logger.getLogger("asd")

    @Test
    fun test() {
        val socket = Socket("irc.chat.twitch.tv", 6667)

        val output = Okio.buffer(Okio.sink(socket))
        val input = Okio.buffer(Okio.source(socket))

        output.writeString("PASS accessToken:1fbwvd2l1fd7z7uvdr9c61114e5iho\r\n", Charset.forName("UTF-8"))
        output.writeString("NICK ukuliii\r\n", Charset.defaultCharset())
        output.flush()

        assertEquals("asd", input.readUtf8Line())
        output.close()
        input.close()
    }

}