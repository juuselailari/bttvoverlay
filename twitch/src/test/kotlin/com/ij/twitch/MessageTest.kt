package com.ij.twitch

import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

/**
 * Created by ilben on 7.4.2017.
 */
class MessageTest {

    @Test
    fun testParseCap() {
        val msg = assertNotNull(Message.parse(":tmi.twitch.tv CAP * ACK :twitch.tv/tags"))
        assertEquals("CAP", msg.command)
        assertEquals("ACK", msg.params[1])
        assertEquals("twitch.tv/tags", msg.params[2])
    }

    @Test
    fun testParseAuth001() {
        val msg = assertNotNull(Message.parse(":tmi.twitch.tv 001 your_username :Welcome, GLHF"))
        assertEquals("001", msg.command)
        assertEquals("your_username", msg.params[0])
    }

    @Test
    fun parseJoin() {
        val msg = assertNotNull(Message.parse(":my_username!my_username@my_username.tmi.twitch.tv JOIN #my_channel"))
        assertEquals("my_username!my_username@my_username.tmi.twitch.tv", msg.prefix)
        assertEquals("JOIN", msg.command)
        assertEquals("#my_channel", msg.params[0])
    }

}