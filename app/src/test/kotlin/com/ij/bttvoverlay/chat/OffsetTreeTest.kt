package com.ij.bttvoverlay.chat

import org.junit.Test
import kotlin.test.assertEquals

/**
 * Created by ilben on 11.4.2017.
 */
class OffsetTreeTest {

    @Test
    fun basic() {
        val tree = OffsetTree(arrayOf(0..2, 3..5), intArrayOf(0, -2))
        assertEquals(0, tree.getOffset(1))
        assertEquals(-2, tree.getOffset(3))
    }

    @Test
    fun empty() {
        val tree = OffsetTree(emptyArray(), intArrayOf())
        assertEquals(-1, tree.getOffset(5))
    }

    @Test
    fun single() {
        val tree = OffsetTree(arrayOf(0..2), intArrayOf(0))
        assertEquals(0, tree.getOffset(1))
    }

}