package com.ij.bttvoverlay.chat

import io.reactivex.Emitter
import io.reactivex.Flowable
import io.reactivex.functions.BiConsumer
import io.reactivex.schedulers.TestScheduler
import org.junit.Test
import java.util.concurrent.Callable
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicInteger
import kotlin.test.assertEquals

/**
 * Created by ilben on 8.4.2017.
 */
class PollObservableTest {

    @Test
    fun test() {
        val generateCount = 5

        val generator = Flowable.generate<String, AtomicInteger>(Callable {
            AtomicInteger(0)
        }, BiConsumer<AtomicInteger, Emitter<String>> { state, emitter ->
            val value = state.getAndIncrement()
            if (value == generateCount) {
                emitter.onComplete()
            } else {
                emitter.onNext(value.toString())
            }
        })

        val scheduler = TestScheduler()
        val running = AtomicBoolean(true)
        val counter = AtomicInteger(0)
        PollObservable(generator)
                .subscribeOn(scheduler)
                .subscribe({
                    counter.incrementAndGet()
                    it.toString()
                }, { }, {
                    running.set(false)
                })

        while (running.get()) {
            scheduler.triggerActions()
        }

        assertEquals(generateCount, counter.get(), "counter.incrementAndGet()")
    }

}