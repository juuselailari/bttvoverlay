package com.ij.bttvoverlay.chat

import com.ij.bttvoverlay.Emoticon
import com.ij.bttvoverlay.EmoticonManager
import com.ij.bttvoverlay.model.Channel
import okhttp3.HttpUrl
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

/**
 * Created by ilben on 10.4.2017.
 */
class ChatPresenterTest {

    @Test
    fun parseUrl() {
        ChatPresenter.parseUrl("https://link.com")
        ChatPresenter.parseUrl("http://link.com")
        ChatPresenter.parseUrl("link.com")
    }

    @Test
    fun parseEmotes() {
        val manager = EmoticonManager()
        val channel = Channel(1, "asd", "asd")
        manager.addGlobalEmotes(arrayOf(Emoticon("LUL".toCharArray(), manager.process("LUL"), emptyArray())))
        manager.setEmotes(channel, arrayOf(Emoticon("MEGALUL".toCharArray(), manager.process("MEGALUL"), emptyArray())))

        val emotes1 = manager.applyEmotes("MEGALUL LUL", channel.id)
        assertEquals(2, emotes1.size)
        assertNotNull(emotes1.find { String(it.emote.pattern) == "MEGALUL" })
        assertNotNull(emotes1.find { String(it.emote.pattern) == "LUL" })

        val emotes2 = manager.applyEmotes("MEGALULLUL", channel.id)
        assertEquals(0, emotes2.size)

        val emotes3 = manager.applyEmotes("MEGALUL LULL", channel.id)
        assertEquals(1, emotes3.size)
        assertNotNull(emotes3.find { String(it.emote.pattern) == "MEGALUL" })

        val emotes4 = manager.applyEmotes("MEGALULL LUL", channel.id)
        assertEquals(1, emotes4.size)
        assertNotNull(emotes4.find { String(it.emote.pattern) == "LUL" })

        val emotes5 = manager.applyEmotes("MEGALULL LUL            MEGALUL", channel.id)
        assertEquals(2, emotes5.size)
        assertNotNull(emotes5.find { String(it.emote.pattern) == "MEGALUL" })
        assertNotNull(emotes5.find { String(it.emote.pattern) == "LUL" })
    }

    @Test
    fun linkOffsetTree() {
        val display = StringBuilder("LUL https://www.twitch.tv/sodapoppin LUL https://www.twitch.tv/sodapoppin LUL https://reddit.com/ LUL")

        val images = ChatPresenter.IMAGE_PATTERN.findAll(display)
                .map {
                    val url = ChatPresenter.parseUrl(it.value)
                    val urlDisplay = ChatPresenter.getDisplayText(url)
                    Image(Url(url, urlDisplay, urlDisplay.length - it.value.length), it.range.start, it.range.endInclusive + 1)
                }
                .toList()
                .toTypedArray()
        val imageStarts = images.map { it.start }.toSet()

        val links = ChatPresenter.URL_PATTERN.findAll(display)
                .filter { !imageStarts.contains(it.range.start) }
                .map {
                    val url = ChatPresenter.parseUrl(it.value)
                    val urlDisplay = ChatPresenter.getDisplayText(url)
                    Link(Url(url, urlDisplay, urlDisplay.length - it.value.length), it.range.start, it.range.endInclusive + 1)
                }
                .toList()
                .toTypedArray()

        val urls = images.map { it.url to it.start..it.end }
                .plus(links.map { it.url to it.start..it.end })
                .sortedBy { it.second.start }
                .asReversed()
                .onEach { display.replace(it.second.start, it.second.last, it.first.display) }
                .asReversed()

        val offsetTree = OffsetTree(urls.map { it.second }.toTypedArray(), OffsetTree.offsets(urls.map { it.first.displayLengthChange }.toIntArray()))
        assertEquals(0, offsetTree.getOffset(1))
        assertEquals(-8, offsetTree.getOffset(38))
        assertEquals(-16, offsetTree.getOffset(75))
        assertEquals(-24, offsetTree.getOffset(99))
    }

    @Test
    fun linkOffsetTree2() {
        val display = StringBuilder("https://www.twitch.tv/sodapoppin a https://www.twitch.tv/sodapoppin a")

        val images = ChatPresenter.IMAGE_PATTERN.findAll(display)
                .map {
                    val url = ChatPresenter.parseUrl(it.value)
                    val urlDisplay = ChatPresenter.getDisplayText(url)
                    Image(Url(url, urlDisplay, urlDisplay.length - it.value.length), it.range.start, it.range.endInclusive + 1)
                }
                .toList()
                .toTypedArray()
        val imageStarts = images.map { it.start }.toSet()

        val links = ChatPresenter.URL_PATTERN.findAll(display)
                .filter { !imageStarts.contains(it.range.start) }
                .map {
                    val url = ChatPresenter.parseUrl(it.value)
                    val urlDisplay = ChatPresenter.getDisplayText(url)
                    Link(Url(url, urlDisplay, urlDisplay.length - it.value.length), it.range.start, it.range.endInclusive + 1)
                }
                .toList()
                .toTypedArray()

        val urls = images.map { it.url to it.start..it.end }
                .plus(links.map { it.url to it.start..it.end })
                .sortedBy { it.second.start }
                .asReversed()
                .onEach { display.replace(it.second.start, it.second.last, it.first.display) }
                .asReversed()

        val offsetTree = OffsetTree(urls.map { it.second }.toTypedArray(), OffsetTree.offsets(urls.map { it.first.displayLengthChange }.toIntArray()))
        assertEquals(0, offsetTree.getOffset(1))
        assertEquals(-8, offsetTree.getOffset(34))
        assertEquals(-16, offsetTree.getOffset(68))
    }

    @Test
    fun linkOffsetTree3() {
        val display = StringBuilder("live in 20 http://i.imgur.com/BXtNijp.png")

        val images = ChatPresenter.IMAGE_PATTERN.findAll(display)
                .map {
                    val url = ChatPresenter.parseUrl(it.value)
                    val urlDisplay = ChatPresenter.getDisplayText(url)
                    Image(Url(url, urlDisplay, urlDisplay.length - it.value.length), it.range.start, it.range.endInclusive + 1)
                }
                .toList()
                .toTypedArray()
        val imageStarts = images.map { it.start }.toSet()

        val links = ChatPresenter.URL_PATTERN.findAll(display)
                .filter { !imageStarts.contains(it.range.start) }
                .map {
                    val url = ChatPresenter.parseUrl(it.value)
                    val urlDisplay = ChatPresenter.getDisplayText(url)
                    Link(Url(url, urlDisplay, urlDisplay.length - it.value.length), it.range.start, it.range.endInclusive + 1)
                }
                .toList()
                .toTypedArray()

        val urls = images.map { it.url to it.start..it.end }
                .plus(links.map { it.url to it.start..it.end })
                .sortedBy { it.second.start }
                .asReversed()
                .onEach { display.replace(it.second.start, it.second.last, it.first.display) }
                .asReversed()

        val offsetTree = OffsetTree(urls.map { it.second }.toTypedArray(), OffsetTree.offsets(urls.map { it.first.displayLengthChange }.toIntArray()))
        assertEquals(0, offsetTree.getOffset(1))
        assertEquals(0, offsetTree.getOffset(8))
        assertEquals(0, offsetTree.getOffset(20))
    }
}

data class Link(val url: Url,
                val start: Int,
                val end: Int)

data class Url(val url: HttpUrl,
               val display: String,
               val displayLengthChange: Int)

data class Image(val url: Url,
                 val start: Int,
                 val end: Int)