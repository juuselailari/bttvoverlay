package com.ij.bttvoverlay.chat

import org.junit.Test
import kotlin.test.assertEquals

/**
 * Created by ilben on 10.4.2017.
 */
class BadgeTest {

    @Test
    fun parseBadge() {
        val badge = Badge.parse("admin/1")
        assertEquals("admin", badge.name)
        assertEquals("1", badge.version)
    }

}