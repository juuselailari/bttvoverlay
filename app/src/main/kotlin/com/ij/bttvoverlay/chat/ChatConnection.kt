package com.ij.bttvoverlay.chat

import android.graphics.Color
import com.ij.bttvoverlay.Auth
import com.ij.bttvoverlay.Option
import com.ij.bttvoverlay.model.Channel
import com.ij.twitch.Capability
import com.ij.twitch.ClientMessage
import com.ij.twitch.MessageReader
import com.ij.twitch.ServerMessage
import io.reactivex.*
import io.reactivex.functions.BiConsumer
import io.reactivex.functions.Consumer
import io.reactivex.internal.subscriptions.SubscriptionArbiter
import io.reactivex.processors.BehaviorProcessor
import org.reactivestreams.Processor
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription
import java.net.Socket
import java.util.*
import java.util.concurrent.Callable
import javax.net.SocketFactory
import javax.net.ssl.SSLSocketFactory
import kotlin.collections.HashMap

/**
 * Created by ilben on 7.4.2017.
 */
class ChatConnection(internal val writeBuffer: Processor<Array<ClientMessage>, Array<ClientMessage>>,
                     internal val readBuffer: Flowable<ServerMessage>) {

    companion object {

        private val CAP_ACK_TO_CAP_MAP = HashMap<String, Capability>()

        init {
            CAP_ACK_TO_CAP_MAP.put("twitch.tv/tags", Capability.Tags)
            CAP_ACK_TO_CAP_MAP.put("twitch.tv/commands", Capability.Commands)
            CAP_ACK_TO_CAP_MAP.put("twitch.tv/membership", Capability.Membership)
        }

        /**
         * Opens a __secure__ connection.
         *
         * Connects to the server using a __SSL socket__
         */
        fun openSecureSocket(): Socket {
            return SSLSocketFactory.getDefault().createSocket("irc.chat.twitch.tv", 443)
        }

        /**
         * Opens a connection.
         *
         * Connects to the server using a socket
         */
        fun openSocket(): Socket {
            return SocketFactory.getDefault().createSocket("irc.chat.twitch.tv", 6667)
        }

        fun listen(reader: MessageReader): Flowable<MessageRead> {
            val readerGenerator = Flowable.generate<ServerMessage, MessageReader>(Callable { reader },
                    BiConsumer<MessageReader, Emitter<ServerMessage>> { reader, emitter ->
                        val msg = reader.readMessage()
                        if (msg != null) {
                            emitter.onNext(msg)
                        }
                    }, Consumer { it.close() })
            return PollObservable(readerGenerator)
                    .map<MessageRead> { MessageRead.Message(it) }
                    .onErrorReturn { MessageRead.Error(it) }
        }

    }

    private val authenticationState = BehaviorProcessor.createDefault<Option<Connection>>(Option.None)

    fun authentications(): Flowable<Option<Connection>> = authenticationState

    fun messages(): Flowable<ChatMessage> {
        return readBuffer
                .map {
                    when (it) {
                        is ServerMessage.Invalid -> {
                            ChatMessage.Invalid(it.line)
                        }
                        is ServerMessage.Valid -> {
                            when (it.message.command) {
                                "PRIVMSG" -> {
                                    val prefix = it.message.prefix
                                    if (prefix == null) {
                                        ChatMessage.Invalid(it.line)
                                    } else {
                                        val nameStart = prefix.indexOf('!') + 1
                                        val nameEnd = prefix.indexOf('@', nameStart)
                                        val user = prefix.substring(nameStart, nameEnd)
                                        val channel = it.message.params[0].removePrefix("#")
                                        val message = it.message.params[1]

                                        val messageContent: MessageContent
                                        if (message[0] == '\u0001' && message.last() == '\u0001') {
                                            val tagEnd = message.indexOf(' ')
                                            val tag = message.substring(1, tagEnd)
                                            messageContent = when (tag) {
                                                "ACTION" -> MessageContent.Action(message.substring(tagEnd + 1, message.length - 1))
                                                else -> MessageContent.Tagged(tag, message.substring(tagEnd + 1, message.length - 1))
                                            }
                                        } else {
                                            messageContent = MessageContent.Simple(message)
                                        }

                                        ChatMessage.PrivMsg(Date(System.currentTimeMillis()), Rich.parse(it.message.tags), user, channel, messageContent)
                                    }
                                }
                                "JOIN" -> ChatMessage.Join(it.message.params[0].removePrefix("#"))
                                "PART" -> ChatMessage.Part(it.message.params[0].removePrefix("#"))
                                "001" -> ChatMessage.Connected
                            //"CAP" -> {
                            //    //when (msg.message.params[1]) {
                            //    //    "ACK" -> {
                            //    //        val enabledCap = msg.message.params[2]
                            //    //    }
                            //    //    else -> {
                            //    //        //TODO: Handle case
                            //    //    }
                            //    //}
//
                            //    ChatMessage.System(it.message.command!!, it.message.params.joinToString(separator = " "))
                            //}
                                null -> ChatMessage.Invalid(it.line)
                                else -> ChatMessage.System(it.message.command!!, it.message.params.joinToString(separator = " "))
                            }
                        }
                    }
                }
    }

    fun authenticate(auth: Auth): Single<Connection> {
        return readBuffer
                .doOnSubscribe { writeBuffer.onNext(arrayOf(ClientMessage.Pass(auth.oauth), ClientMessage.Nick(auth.username))) }
                .filter { it is ServerMessage.Valid }
                .map { it as ServerMessage.Valid }
                .filter { it.message.command == "001" && it.message.params[0] == auth.username }
                .map { Connection(this, auth) }
                .doOnNext { authenticationState.onNext(Option.Some(it)) }
                .singleOrError()
    }

    /**
     * Returns observable which upon subscribing sends a request for enabling
     * the [capabilities].
     *
     * Returned observable emits the capabilities one by one as server enables them
     *
     * @param capabilities array of [capabilities][Capability] to enable
     */
    fun requestCapabilities(capabilities: Array<Capability>): Flowable<Capability> {
        return readBuffer
                .doOnSubscribe { writeBuffer.onNext(capabilities.map { ClientMessage.CapReq(it) }.toTypedArray()) }
                .filter { it is ServerMessage.Valid }
                .map { it as ServerMessage.Valid }
                .filter {
                    val msg = it.message
                    when (msg.command) {
                        "CAP" -> {
                            when (msg.params[1]) {
                                "ACK" -> CAP_ACK_TO_CAP_MAP.contains(msg.params[2])
                                else -> false
                            }
                        }
                        else -> false
                    }
                }
                .map { CAP_ACK_TO_CAP_MAP[it.message.params[2]]!! }
    }

    fun writeMessages(messages: Array<ClientMessage>) {
        writeBuffer.onNext(messages)
    }

}

sealed class MessageRead {
    class Message(val message: ServerMessage) : MessageRead()
    class Error(val throwable: Throwable) : MessageRead()
}

sealed class MessageContent {
    data class Simple(val message: String) : MessageContent()
    data class Action(val message: String) : MessageContent()
    data class Tagged(val tag: String, val message: String) : MessageContent()
}

class Room(val channel: Channel) {

    internal val state = BehaviorProcessor.createDefault(RoomState.CONNECTING)
    fun state(): Flowable<RoomState> = state

}

enum class RoomState {
    CONNECTING,
    CONNECTED,
    DISCONNECTING,
    DISCONNECTED
}

class Connection(val chatConnection: ChatConnection,
                 val auth: Auth) {

    private val rooms = HashMap<String, Room>()
    private val roomsObserver = BehaviorProcessor.createDefault<Array<Room>>(arrayOf())

    fun rooms(): Flowable<Array<Room>> = roomsObserver

    fun roomState(channel: Channel): Flowable<RoomState> {
        return roomsObserver.flatMap {
            val room = it.find { it.channel.name == channel.name }
            room?.state() ?: Flowable.never()
        }
    }

    /**
     * Returns completable which upon subscribing sends the request.
     *
     * Completable returns when server responds
     */
    fun joinChannel(channel: Channel): Completable {
        return chatConnection.readBuffer
                .doOnSubscribe { chatConnection.writeBuffer.onNext(arrayOf(ClientMessage.Join(channel.name))) }
                .filter { it.line == ":${auth.username}!${auth.username}@${auth.username}.tmi.twitch.tv JOIN #$channel" }
                .flatMapCompletable({ Completable.complete() })
    }

    fun joinRoom(channel: Channel): Completable {
        if (rooms.contains(channel.name)) return Completable.complete()
        val room = Room(channel)
        return chatConnection.readBuffer
                .doOnSubscribe {
                    addRoom(room)
                    room.state.onNext(RoomState.CONNECTING)
                    chatConnection.writeMessages(arrayOf(ClientMessage.Join(room.channel.name)))
                }
                .filter { it.line == ":${auth.username}!${auth.username}@${auth.username}.tmi.twitch.tv JOIN #${channel.name}" }
                .doOnNext { room.state.onNext(RoomState.CONNECTED) }
                .flatMapCompletable { Completable.complete() }
    }

    fun partRoom(channel: Channel): Completable {
        val room = rooms[channel.name] ?: return Completable.complete()
        return chatConnection.readBuffer
                .doOnSubscribe {
                    room.state.onNext(RoomState.DISCONNECTING)
                    chatConnection.writeMessages(arrayOf(ClientMessage.Part(channel.name)))
                }
                .filter { it.line == ":${auth.username}!${auth.username}@${auth.username}.tmi.twitch.tv PART #${channel.name}" }
                .doOnNext {
                    room.state.onNext(RoomState.DISCONNECTED)
                    removeRoom(room)
                }
                .flatMapCompletable { Completable.complete() }
    }

    private fun addRoom(room: Room) {
        rooms.put(room.channel.name, room)
        roomsObserver.onNext(rooms.values.toTypedArray())
    }

    private fun removeRoom(room: Room) {
        rooms.remove(room.channel.name)
        roomsObserver.onNext(rooms.values.toTypedArray())
    }

}

class PollObservable<T>(val source: Flowable<T>) : Flowable<T>() {
    override fun subscribeActual(s: Subscriber<in T>) {
        val observer = PollObserver(s)
        s.onSubscribe(observer.arbiter)
        source.subscribe(observer)
    }

    private class PollObserver<T>(val actual: Subscriber<T>) : FlowableSubscriber<T> {
        val arbiter = SubscriptionArbiter()

        override fun onNext(value: T) {
            actual.onNext(value)
            if (!arbiter.isCancelled) {
                arbiter.request(1)
            }
        }

        override fun onComplete() {
            actual.onComplete()
        }

        override fun onSubscribe(s: Subscription) {
            arbiter.setSubscription(s)
        }

        override fun onError(e: Throwable) {
            actual.onError(e)
        }

    }

}

sealed class ChatMessage {
    data class Invalid(val line: String) : ChatMessage()
    data class System(val action: String, val message: String) : ChatMessage()
    data class Join(val roomName: String) : ChatMessage()
    data class Part(val roomName: String) : ChatMessage()
    data class PrivMsg(val received: Date, val rich: Rich, val user: String, val roomName: String, val message: MessageContent) : ChatMessage()
    object Connected : ChatMessage()
    //class Disconnected() : ChatMessage()
}

data class Rich(
        val roomId: Long?,
        val badges: Array<Badge>,
        val globalMod: Boolean,
        val staff: Boolean,
        val turbo: Boolean,
        val mod: Boolean,
        val subscriber: Boolean,
        val broadcaster: Boolean,
        val color: Int?,
        val emotes: Array<EmoteSpans>?,
        val sent: Date?) {

    companion object {

        fun parse(tags: HashMap<String, String>): Rich {
            val emoteString = tags["emotes"]
            val emotes = if (emoteString == null) {
                emptyArray<EmoteSpans>()
            } else {
                EmoteSpans.parse(emoteString)
            }

            val colorString = tags["color"]
            val color = if (colorString == null || colorString.isEmpty()) null else Color.parseColor(colorString)

            val sendTsString = tags["tmi-sent-ts"]
            val sent = if (sendTsString == null) null else Date(sendTsString.toLong())

            val badges = tags["badges"]?.split(',')
                    ?.map { if (it.isEmpty()) null else it }
                    ?.filterNotNull()?.map { Badge.parse(it) }
                    ?.toTypedArray() ?: emptyArray()
            return Rich(
                    tags["room-id"]?.toLong(),
                    badges,
                    tags["global_mod"]?.equals("1") ?: false,
                    tags["staff"]?.equals("1") ?: false,
                    tags["turbo"]?.equals("1") ?: false,
                    tags["mod"]?.equals("1") ?: false,
                    tags["subscriber"]?.equals("1") ?: false,
                    tags["broadcaster"]?.equals("1") ?: false,
                    color,
                    emotes,
                    sent)
        }

    }

}

data class Badge(val name: String, val version: String) {
    companion object {
        fun parse(badge: String): Badge {
            val slash = badge.indexOf('/')
            val name = badge.substring(0, slash)
            val version = badge.substring(slash + 1, badge.length)
            return Badge(name, version)
        }
    }
}

data class EmoteSpans(val emoteId: Int,
                      val starts: IntArray,
                      val ends: IntArray) {

    companion object {

        fun parse(line: String): Array<EmoteSpans> {
            return line.split('/')
                    .map {
                        val emoteIdStart = 0
                        val emoteIdEnd = it.indexOf(':')
                        if (emoteIdEnd < 0) {
                            null
                        } else {
                            val emoteId = it.substring(emoteIdStart, emoteIdEnd).toInt()

                            val ranges = it.substring(emoteIdEnd + 1)
                                    .split(',')
                            val starts = IntArray(ranges.size)
                            val ends = IntArray(ranges.size)
                            ranges.forEachIndexed { index, s ->
                                val separatorPos = s.indexOf('-')
                                val start = s.substring(0, separatorPos).toInt()
                                val end = s.substring(separatorPos + 1).toInt()
                                starts[index] = start
                                ends[index] = end
                            }

                            EmoteSpans(emoteId, starts, ends)
                        }
                    }
                    .filterNotNull()
                    .toTypedArray()
        }

    }

}