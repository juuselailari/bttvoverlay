package com.ij.bttvoverlay.chat

import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.content.ServiceConnection
import android.graphics.Color
import android.os.Binder
import android.os.Handler
import android.os.IBinder
import android.support.v4.app.ServiceCompat
import android.support.v7.app.NotificationCompat
import android.util.Log
import com.ij.bttvoverlay.*
import com.ij.bttvoverlay.api.BttvApi
import com.ij.bttvoverlay.model.ImageUrl
import com.ij.twitch.ClientMessage
import com.ij.twitch.MessageReader
import com.ij.twitch.MessageWriter
import com.ij.twitch.ServerMessage
import dagger.Binds
import dagger.android.AndroidInjector
import dagger.android.DaggerService
import dagger.android.ServiceKey
import dagger.multibindings.IntoMap
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.processors.PublishProcessor
import io.reactivex.schedulers.Schedulers
import okhttp3.HttpUrl
import okio.Okio
import java.net.Socket
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Service which manages the active socket connection.
 *
 * This service should be used by started with [startService] and then connecting
 * to it with [bindService]. This setup allows this service to survive configuration changes
 * and to be notified when no active [ServiceConnection] exists. With these rules this service
 * can safely manage its own lifetime.
 *
 * Created by ilben on 21.4.2017.
 */
class ConnectionService : DaggerService() {

    @dagger.Subcomponent(modules = arrayOf())
    interface Subcomponent : AndroidInjector<ConnectionService> {
        @dagger.Subcomponent.Builder
        abstract class Builder : AndroidInjector.Builder<ConnectionService>()
    }

    @dagger.Module(subcomponents = arrayOf(Subcomponent::class))
    abstract class Module {
        @Binds
        @IntoMap
        @ServiceKey(ConnectionService::class)
        abstract fun bindInjectorFactory(builder: Subcomponent.Builder): AndroidInjector.Factory<out Service>
    }

    @dagger.Component(modules = arrayOf(Module::class))
    interface Component

    companion object {
        /**
         * Time in milliseconds for how long the service stays alive without
         * having anyone connected to it via [bindService]
         */
        private val UNBOUNDED_LIFETIME_DURATION = TimeUnit.MILLISECONDS.convert(30, TimeUnit.SECONDS)
    }

    private val subscriptions = CompositeDisposable()

    private val handler = Handler()
    private val delayedShutdown = Runnable { stopSelf() }

    private val appIsForeground = BehaviorProcessor.createDefault(true)

    private val writeScheduler = Schedulers.from(Executors.newSingleThreadScheduledExecutor())
    private val readScheduler = Schedulers.from(Executors.newSingleThreadScheduledExecutor())

    @Inject
    lateinit var bttvApi: BttvApi

    @Inject
    lateinit var emoticonManager: EmoticonManager

    @Inject
    lateinit var connectionManager: ConnectionManager

    @Inject
    lateinit var authenticationManager: AuthenticationManager

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        cancelShutDown()
        return Service.START_STICKY
    }

    @Suppress("IfThenToElvis", "UNCHECKED_CAST")
    override fun onCreate() {
        super.onCreate()

        subscriptions.add(bttvApi.emotes()
                .map {
                    val template = it.urlTemplate
                    it.emotes.map {
                        try {
                            val processed = emoticonManager.process(it.code)

                            val image1x = ImageUrl(HttpUrl.parse("https:" + UrlUtils.applyToUrlTemplate(template, it.id, "1x")), 1f)
                            val image2x = ImageUrl(HttpUrl.parse("https:" + UrlUtils.applyToUrlTemplate(template, it.id, "2x")), 2f)
                            val image4x = ImageUrl(HttpUrl.parse("https:" + UrlUtils.applyToUrlTemplate(template, it.id, "3x")), 4f)
                            val imageUrls = arrayOf(image1x, image2x, image4x)
                            Emoticon(it.code.toCharArray(), processed, arrayOf(Image(imageUrls)))
                        } catch (e: Throwable) {
                            null
                        }
                    }.filterNotNull()
                }
                .subscribe({
                    emoticonManager.addGlobalEmotes(it.toTypedArray())
                }, {
                    throw it
                }))

        val socketChanges = PublishProcessor.create<Int>()
        val conn = socketChanges
                .flatMap {
                    Flowable.fromCallable<OpenSocket> { OpenSocket.Success(ChatConnection.openSocket()) }
                            .onErrorReturn { OpenSocket.Error(it) }
                            .flatMap {
                                when (it) {
                                    is OpenSocket.Success -> Flowable.using({ MessageReader(Okio.buffer(Okio.source(it.socket))) to MessageWriter(Okio.buffer(Okio.sink(it.socket))) },
                                            { BehaviorProcessor.createDefault(it) },
                                            { it.first.close(); it.second.close() })
                                            .map<PrepareSocket> { PrepareSocket.Success(it) }
                                            .onErrorReturn { PrepareSocket.Error(it) }
                                    is OpenSocket.Error -> Flowable.just(PrepareSocket.Error(it.throwable))
                                }
                            }
                            .subscribeOn(Schedulers.io())
                }
                .share()

        val messages = conn
                .flatMap {
                    when (it) {
                        is PrepareSocket.Success -> ChatConnection.listen(it.prepared.first).observeOn(readScheduler)
                        is PrepareSocket.Error -> Flowable.never()
                    }
                }
                .share()

        val writeBuffer = conn.map { PublishProcessor.create<Array<ClientMessage>>() }.share()

        subscriptions.add(Flowable.combineLatest(arrayOf(writeBuffer.flatMap { it }, conn),
                { it[1] as PrepareSocket to it[0] as Array<ClientMessage> })
                .observeOn(writeScheduler)
                .subscribe {
                    val state = it.first
                    when (state) {
                        is PrepareSocket.Success -> state.prepared.second.writeMessages(it.second)
                        is PrepareSocket.Error -> Log.d("ConnectionService", "Connection error: ${state.throwable}")
                    }
                })

        val noErrorsMessages = messages.filter { it is MessageRead.Message }
                .map { (it as MessageRead.Message).message }
                .share()

        subscriptions.add(writeBuffer
                .map<ChatState> { ChatState.Connected(ChatConnection(it, noErrorsMessages)) }
                .onErrorReturn { ChatState.Error(it) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { connectionManager.switchConnection(it) })

        subscriptions.add(Flowable.combineLatest(arrayOf(writeBuffer, noErrorsMessages), {
            it[0] as PublishProcessor<Array<ClientMessage>> to it[1] as ServerMessage
        }).subscribe {
            val writer = it.first
            val msg = it.second

            when (msg) {
                is ServerMessage.Valid -> {
                    when (msg.message.command) {
                        "PING" -> writer.onNext(arrayOf(ClientMessage.Pong()))
                    }
                }
            }
        })

        subscriptions.add(Flowable.combineLatest(arrayOf(connectionManager.connection(),
                authenticationManager.authentication()), {
            it[0] as ChatState to it[1] as Option<Auth>
        })
                .flatMap {
                    val chatState = it.first
                    val auth = it.second

                    when (chatState) {
                        is ChatState.Connected -> {
                            when (auth) {
                                Option.None -> Flowable.never()
                                is Option.Some -> {
                                    chatState.chatConnection.authenticate(auth.value)
                                            .toFlowable()
                                            .map { auth.value }
                                }
                            }
                        }
                        else -> Flowable.empty()
                    }
                }
                .subscribe())

        socketChanges.onNext(0)

        val rooms = connectionManager.connection()
                .flatMap { if (it is ChatState.Connected) it.chatConnection.authentications() else Flowable.just(Option.None) }
                .flatMap { if (it is Option.Some) it.value.rooms() else Flowable.just(emptyArray()) }

        subscriptions.add(Flowable.combineLatest(arrayOf(appIsForeground.distinctUntilChanged(), rooms), { it[0] as Boolean to it[1] as Array<Room> })
                .subscribe { (appIsForeground, rooms) ->
                    if (appIsForeground) hideForegroundNotification() else showForegroundNotification(rooms)
                })

    }

    override fun onDestroy() {
        super.onDestroy()

        ServiceCompat.stopForeground(this, ServiceCompat.STOP_FOREGROUND_REMOVE)
        subscriptions.clear()
    }

    override fun onBind(intent: Intent?): IBinder? {
        cancelShutDown()
        return ConnectionInterface()
    }

    override fun onRebind(intent: Intent?) {
        super.onRebind(intent)

        cancelShutDown()
    }

    /**
     * Called when all clients have unbound.
     * @param intent        The first intent originally passed on binding
     * *
     * @return Whether this service should be notified of rebinding
     */
    override fun onUnbind(intent: Intent): Boolean {
        handler.postDelayed(delayedShutdown, UNBOUNDED_LIFETIME_DURATION)

        // we do want onRebind called when clients return
        return true
    }

    private fun cancelShutDown() {
        handler.removeCallbacks(delayedShutdown)
    }

    fun onAppOnForeground() {
        appIsForeground.onNext(true)
    }

    fun onAppOnBackground() {
        appIsForeground.onNext(false)
    }

    private fun showForegroundNotification(rooms: Array<Room>) {
        val chatIntent = MainActivity.createOpenChatIntent(this)
        val contentIntent = PendingIntent.getActivity(this, R.id.notification_code_open_chat, chatIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val notification = NotificationCompat.Builder(this)
                .setColor(getAttributeColor(ATTRS_COLOR_ACCENT, Color.RED))
                .setOngoing(true)
                .setShowWhen(false)
                .setContentIntent(contentIntent)
                .setContentTitle(getText(R.string.service_connection_open))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setTicker(getText(R.string.service_connection_open))
                .apply { if (rooms.isNotEmpty()) setContentText(rooms.joinToString { it.channel.name }) }
                .build()

        startForeground(R.id.notification_chat, notification)
    }

    fun hideForegroundNotification() {
        ServiceCompat.stopForeground(this, ServiceCompat.STOP_FOREGROUND_REMOVE)
    }

    inner class ConnectionInterface : Binder() {

        fun onAppOnBackground() {
            this@ConnectionService.onAppOnBackground()
        }

        fun onAppOnForeground() {
            this@ConnectionService.onAppOnForeground()
        }

    }

}

sealed class OpenSocket {
    class Success(val socket: Socket) : OpenSocket()
    class Error(val throwable: Throwable) : OpenSocket()
}

sealed class PrepareSocket {
    class Success(val prepared: Pair<MessageReader, MessageWriter>) : PrepareSocket()
    class Error(val throwable: Throwable) : PrepareSocket()
}

sealed class SocketState {
    class Connected(val socket: Socket) : SocketState()
    class Error(val throwable: Throwable) : SocketState()
}

sealed class ChatState {
    class Connected(val chatConnection: ChatConnection) : ChatState()
    class Disconnected() : ChatState()
    class Error(val throwable: Throwable) : ChatState()
}