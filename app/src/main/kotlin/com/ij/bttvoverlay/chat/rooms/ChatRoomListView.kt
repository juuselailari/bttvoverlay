package com.ij.bttvoverlay.chat.rooms

import android.content.Context
import android.support.v7.util.DiffUtil
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.view.ViewManager
import com.ij.bttvoverlay.chat.rooms.ChatRoomListView.Companion.ANKO_FACTORY
import com.ij.bttvoverlay.model.Channel
import org.jetbrains.anko.*
import org.jetbrains.anko.custom.ankoView
import java.text.Collator

/**
 * Created by ilben on 13.4.2017.
 */
class ChatRoomListView(context: Context) : RecyclerView(context) {

    companion object {
        val ANKO_FACTORY = { ctx: Context -> ChatRoomListView(ctx) }
    }

    private val listAdapter = Adapter(context)

    init {
        layoutManager = LinearLayoutManager(context)
        adapter = listAdapter
    }

    fun setRooms(rooms: Array<Channel>) {
        listAdapter.setRooms(rooms)
    }

    private class Adapter(context: Context) : RecyclerView.Adapter<ViewHolder>() {

        private val ankoContext = AnkoContext.createReusable(context)
        private val rooms = ArrayList<Channel>()

        private val collator = Collator.getInstance()
        private val roomNameComparator = Comparator<Channel> { o1, o2 -> collator.compare(o1.name, o2.name) }

        fun setRooms(rooms: Array<Channel>) {
            rooms.sortWith(roomNameComparator)
            val diff = DiffUtil.calculateDiff(RoomDiffCallback(this.rooms, rooms), true)
            this.rooms.clear()
            this.rooms.addAll(rooms)
            diff.dispatchUpdatesTo(this)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val room = rooms[position]
            holder.roomView.setRoom(room)
        }

        override fun getItemCount(): Int {
            return rooms.size
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(RoomViewUi.createView(ankoContext))
        }

    }

    private class ViewHolder(val roomView: RoomView) : RecyclerView.ViewHolder(roomView)

    object RoomViewUi : AnkoComponent<Context> {
        override fun createView(ui: AnkoContext<Context>): RoomView = with(ui) {
            RoomView(this.ctx).apply {
                verticalPadding = dip(8)
                clipToPadding = false
                layoutParams = RecyclerView.LayoutParams(matchParent, wrapContent)
            }
        }
    }
}

private class RoomDiffCallback(val oldItems: List<Channel>, val newItems: Array<Channel>) : DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldItems[oldItemPosition].name == newItems[newItemPosition].name
    }

    override fun getOldListSize(): Int = oldItems.size

    override fun getNewListSize(): Int = newItems.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldItems[oldItemPosition] == newItems[newItemPosition]
    }

}

inline fun ViewManager.chatRoomListView(theme: Int = 0, init: ChatRoomListView.() -> Unit): ChatRoomListView {
    return ankoView(ANKO_FACTORY, theme) { init() }
}