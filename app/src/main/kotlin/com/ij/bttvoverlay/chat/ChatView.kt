package com.ij.bttvoverlay.chat

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import android.view.ViewManager
import com.ij.bttvoverlay.model.Message
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import org.jetbrains.anko.custom.ankoView

/**
 * Created by ilben on 5.4.2017.
 */
class ChatView(context: Context) : ChatListView(context) {
    companion object {
        val ANKO_FACTORY = { ctx: Context -> ChatView(ctx) }
        const val MAX_MESSAGE_COUNT = 100
        const val REMOVE_BATCH_SIZE = 10
    }

    private val subscriptions = CompositeDisposable()
    private val itemAdds = PublishSubject.create<List<ChatListContract.Message>>()
    private var messageIdGenerator = 0L

    private fun generateMessageId(): Long {
        if (messageIdGenerator > 10000) {
            messageIdGenerator = 0
        } else {
            messageIdGenerator += 1
        }

        return messageIdGenerator
    }

    init {
        keepScreenOn = true
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        subscriptions.add(itemAdds
                .map {
                    it.map<ChatListContract.Message, ChatItem> {
                        when (it) {
                            is ChatListContract.Message.Invalid -> ChatItem.Invalid(generateMessageId(), Message.Invalid(it.line))
                            is ChatListContract.Message.PrivMsg -> ChatItem.PrivMsg(generateMessageId(), Message.PrivMsg(it.received,
                                    it.sent,
                                    Message.User(it.user.name, it.user.host, it.user.color),
                                    it.roomName,
                                    it.message,
                                    it.isAction,
                                    it.emotes.map { Message.Emote(it.urls.map { Message.ImageUrl(it.url, it.scale) }.toTypedArray(), it.width, it.height, it.starts, it.ends) }.toTypedArray(),
                                    it.badges.map { Message.Badge(it.urls.map { Message.ImageUrl(it.url, it.scale) }.toTypedArray(), it.title, it.description) }.toTypedArray(),
                                    it.mentions.map { Message.Mention(it.host, it.start, it.end) }.toTypedArray(),
                                    it.images.map { Message.Image(Message.Url(it.url.url, it.url.display, it.url.displayLengthChange), it.start, it.end) }.toTypedArray(),
                                    it.links.map { Message.Link(Message.Url(it.url.url, it.url.display, it.url.displayLengthChange), it.start, it.end) }.toTypedArray()))
                            is ChatListContract.Message.Connect -> ChatItem.Connect(generateMessageId(), Message.Connect())
                            is ChatListContract.Message.Join -> ChatItem.Join(generateMessageId(), Message.Join(it.name))
                            is ChatListContract.Message.Part -> ChatItem.Part(generateMessageId(), Message.Part(it.name))
                        }
                    }
                }
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { doAddMessages(it) })
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()

        subscriptions.clear()
    }

    fun addMessages(messages: List<ChatListContract.Message>) {
        itemAdds.onNext(messages)
    }

    private fun doAddMessages(messages: List<ChatItem>) {
        if (messages.size < MAX_MESSAGE_COUNT) {
            val itemCount = adapter.itemCount

            val delta = itemCount + messages.size - MAX_MESSAGE_COUNT
            if (delta > 0) {
                if (delta > REMOVE_BATCH_SIZE) {
                    removeItems(itemCount - delta, delta)
                } else {
                    val d = REMOVE_BATCH_SIZE.coerceAtMost(itemCount)
                    removeItems(itemCount - d, d)
                }
            }

            addItems(0, messages)
        } else {
            removeItems(0, MAX_MESSAGE_COUNT)
            // Newest items are first so take from the beginning of the list
            addItems(0, messages.take(MAX_MESSAGE_COUNT))
        }

        // Keep the view scrolled to the bottom
        scrollToPosition(0)
    }

    override fun onSaveInstanceState(): Parcelable {
        return SavedInstanceState(super.onSaveInstanceState(), messageIdGenerator)
    }

    override fun onRestoreInstanceState(state: Parcelable) {
        val s = state as SavedInstanceState
        super.onRestoreInstanceState(s.superState)
        messageIdGenerator = s.messageId
    }

    @Suppress("UNCHECKED_CAST")
    class SavedInstanceState : BaseSavedState {

        companion object {
            @JvmField val CREATOR = object : Parcelable.Creator<SavedInstanceState> {
                override fun createFromParcel(source: Parcel): SavedInstanceState {
                    return SavedInstanceState(source)
                }

                override fun newArray(size: Int): Array<SavedInstanceState?> {
                    return arrayOfNulls(size)
                }
            }
        }

        val messageId: Long

        constructor(source: Parcel) : super(source) {
            messageId = source.readLong()
        }

        constructor(source: Parcel, loader: ClassLoader?) : super(source, loader) {
            messageId = source.readLong()
        }

        constructor(superState: Parcelable, messageId: Long) : super(superState) {
            this.messageId = messageId
        }

        override fun writeToParcel(out: Parcel, flags: Int) {
            super.writeToParcel(out, flags)

            out.writeLong(messageId)
        }
    }
}

inline fun ViewManager.chatView(theme: Int = 0, init: ChatView.() -> Unit): ChatView {
    return ankoView(ChatView.ANKO_FACTORY, theme) { init() }
}