package com.ij.bttvoverlay.chat

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ij.bttvoverlay.chat.badge.BadgeListView
import com.ij.bttvoverlay.model.Message
import org.jetbrains.anko.AnkoComponent
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.support.v4.nestedScrollView
import org.jetbrains.anko.support.v4.withArguments

/**
 * Created by ilben on 24.4.2017.
 */
class MessageInfoFragment : AppCompatDialogFragment() {

    companion object {
        const val TAG = "tag:mesage_info_fragment"
        private const val ARG_MESSAGE = "arg:message"

        fun create(message: Message.PrivMsg): MessageInfoFragment {
            return MessageInfoFragment()
                    .withArguments(ARG_MESSAGE to message)
        }
    }

    private lateinit var message: Message.PrivMsg

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        message = arguments.getParcelable(ARG_MESSAGE)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val badgeView = BadgeListView(context)
        badgeView.setBadges(message.badges)
        return AlertDialog.Builder(context)
                .setTitle(message.user.name)
                .setView(badgeView)
                .create()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return UserInfoFragmentUi.createView(AnkoContext.create(context))
    }

}

object UserInfoFragmentUi : AnkoComponent<Context> {
    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        nestedScrollView {
            layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)
        }
    }

}