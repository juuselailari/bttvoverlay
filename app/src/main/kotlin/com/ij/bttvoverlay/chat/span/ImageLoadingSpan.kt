package com.ij.bttvoverlay.chat.span

import org.jetbrains.anko.withAlpha

/**
 * Created by ilben on 6.4.2017.
 */
class ImageLoadingSpan(val width: Int,
                       val height: Int) : android.text.style.ReplacementSpan() {
    override fun getSize(paint: android.graphics.Paint?, text: CharSequence?, start: Int, end: Int, fm: android.graphics.Paint.FontMetricsInt?): Int {
        if (fm != null) {
            fm.ascent = -height
            fm.descent = 0

            fm.top = fm.ascent
            fm.bottom = 0
        }

        return width
    }

    override fun draw(canvas: android.graphics.Canvas?, text: CharSequence?, start: Int, end: Int, x: Float, top: Int, y: Int, bottom: Int, paint: android.graphics.Paint?) {
        paint?.color = android.graphics.Color.BLACK.withAlpha((0.1f * 255).toInt())
        canvas?.drawRect(x, top.toFloat(), x + width, bottom.toFloat(), paint)
    }
}