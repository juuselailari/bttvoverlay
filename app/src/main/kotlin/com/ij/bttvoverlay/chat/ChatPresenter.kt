package com.ij.bttvoverlay.chat

import android.graphics.Color
import android.support.v4.util.PatternsCompat
import android.support.v4.util.SparseArrayCompat
import android.util.Log
import com.ij.bttvoverlay.*
import com.ij.bttvoverlay.api.BttvApi
import com.ij.bttvoverlay.api.TwitchApi
import com.ij.bttvoverlay.model.BadgeSets
import com.ij.bttvoverlay.model.Channel
import com.ij.bttvoverlay.model.ImageUrl
import com.ij.twitch.Capability
import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.processors.PublishProcessor
import okhttp3.HttpUrl
import org.jetbrains.anko.append
import org.jetbrains.anko.buildSpanned
import org.jetbrains.anko.foregroundColor
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton


/**
 * Created by ilben on 5.4.2017.
 */
@Singleton
class ChatPresenter @Inject constructor(private val connectionManager: ConnectionManager,
                                        private val emoteManager: EmoticonManager,
                                        private val chatPreferenceManager: ChatPreferenceManager,
                                        private val twitchApi: TwitchApi,
                                        private val bttvApi: BttvApi,
                                        @MainThreadScheduler private val mainThreadScheduler: Scheduler,
                                        @IoScheduler private val ioScheduler: Scheduler) : ChatListContract.Presenter {

    companion object {
        private const val MESSAGE_BUFFER_MS = 150L
        internal val MENTION_PATTERN = Regex("(?<!\\w)@\\w+")
        internal val IMAGE_PATTERN = Regex("(https?://.)([a-z-_0-9/:.%+]*.(jpg|jpeg|png|gif|gifv|webm))", RegexOption.IGNORE_CASE)
        internal val URL_PATTERN = PatternsCompat.AUTOLINK_WEB_URL.toRegex()

        fun parseUrl(url: String): HttpUrl {
            return url.let { if (isHttpUrl(it) || isHttpsUrl(it)) it else "https://$it" }
                    .let { HttpUrl.parse(it) }
        }

        /**
         * @return True iff the url is an http: url.
         */
        private fun isHttpUrl(url: String?): Boolean {
            return null != url &&
                    url.length > 6 &&
                    url.substring(0, 7).equals("http://", ignoreCase = true)
        }

        /**
         * @return True iff the url is an https: url.
         */
        private fun isHttpsUrl(url: String?): Boolean {
            return null != url &&
                    url.length > 7 &&
                    url.substring(0, 8).equals("https://", ignoreCase = true)
        }

        internal fun getDisplayText(url: HttpUrl): String {
            val path = url.pathSegments().joinToString(separator = "/")
            return url.host() +
                    if (path.isNotEmpty()) {
                        "/"
                    } else {
                        ""
                    } +
                    path +
                    url.query().let { if (it.isNullOrEmpty()) "" else "?$it" } +
                    url.fragment().let { if (it.isNullOrEmpty()) "" else "#$it" }
        }

    }

    private var view: ChatListContract.View? = null

    private val roomJoins = PublishProcessor.create<Channel>()
    private val roomParts = PublishProcessor.create<Channel>()
    private val subscriptions = CompositeDisposable()

    @Suppress("UNCHECKED_CAST")
    override fun attach(view: ChatListContract.View) {
        this.view = view

        subscriptions.add(connectionManager.connection()
                .flatMap { (it as? ChatState.Connected)?.chatConnection?.authentications() ?: Flowable.just(Option.None) }
                .flatMap { (it as? Option.Some)?.value?.rooms()?.map { Option.Some(it) } ?: Flowable.just(Option.None) }
                .observeOn(mainThreadScheduler)
                .subscribe {
                    val rooms = when (it) {
                        Option.None -> emptyArray()
                        is Option.Some -> it.value.map { it.channel }.toTypedArray()
                    }
                    view.setRooms(rooms)
                }
        )

        val authenticatedConnections = connectionManager.connection()
                .flatMap { (it as? ChatState.Connected)?.chatConnection?.authentications() ?: Flowable.just(Option.None) }
                .share()

        subscriptions.add(Flowable.combineLatest(arrayOf(roomJoins, authenticatedConnections), {
            val roomName = it[0] as Channel
            val conn = it[1] as Option<Connection>
            roomName to conn
        }).flatMap {
            val conn = it.second
            when (conn) {
                Option.None -> Flowable.never()
                is Option.Some -> conn.value.joinRoom(it.first).toFlowable<Unit>()
            }
        }
                .observeOn(mainThreadScheduler)
                .subscribe())

        subscriptions.add(Flowable.combineLatest(arrayOf(roomParts, authenticatedConnections), {
            val roomName = it[0] as Channel
            val conn = it[1] as Option<Connection>
            roomName to conn
        }).flatMap {
            val conn = it.second
            when (conn) {
                Option.None -> Flowable.never()
                is Option.Some -> conn.value.partRoom(it.first).toFlowable<Unit>()
            }
        }
                .observeOn(mainThreadScheduler)
                .subscribe())

        // Subscription which reads messages
        subscriptions.add(connectionManager.connection()
                .flatMap { (it as? ChatState.Connected)?.chatConnection?.messages() ?: Flowable.never() }
                .map {
                    val msg = it
                    when (msg) {
                        is ChatMessage.Invalid -> {
                            ChatListContract.Message.Invalid(msg.line)
                        }
                        is ChatMessage.System -> {
                            val display = buildSpanned {
                                append(msg.action, foregroundColor(Color.GRAY))
                                append(" ")
                                append(msg.message)
                            }
                            ChatListContract.Message.Invalid(display.toString())
                        }
                        is ChatMessage.Join -> {
                            ChatListContract.Message.Join(msg.roomName)
                        }
                        is ChatMessage.Part -> {
                            ChatListContract.Message.Part(msg.roomName)
                        }
                        ChatMessage.Connected -> {
                            ChatListContract.Message.Connect()
                        }
                        is ChatMessage.PrivMsg -> {
                            val messageToIsAction = when (msg.message) {
                                is MessageContent.Simple -> msg.message.message to false
                                is MessageContent.Action -> msg.message.message to true
                                is MessageContent.Tagged -> msg.message.message to true
                            }

                            val channel = msg.roomName
                            val messageBuilder = StringBuilder(messageToIsAction.first)

                            val images = ChatPresenter.IMAGE_PATTERN.findAll(messageBuilder)
                                    .map {
                                        val url = ChatPresenter.parseUrl(it.value)
                                        val urlDisplay = ChatPresenter.getDisplayText(url)
                                        ChatListContract.Image(ChatListContract.Url(url, urlDisplay, urlDisplay.length - it.value.length), it.range.start, it.range.start + urlDisplay.length)
                                    }
                                    .toList()
                                    .toTypedArray()
                            val imageStarts = images.map { it.start }.toSet()

                            val links = ChatPresenter.URL_PATTERN.findAll(messageBuilder)
                                    .filter { !imageStarts.contains(it.range.start) }
                                    .map {
                                        val url = ChatPresenter.parseUrl(it.value)
                                        val urlDisplay = ChatPresenter.getDisplayText(url)
                                        ChatListContract.Link(ChatListContract.Url(url, urlDisplay, urlDisplay.length - it.value.length), it.range.start, it.range.start + urlDisplay.length)
                                    }
                                    .toList()
                                    .toTypedArray()

                            val urls = images.map { it.url to it.start..it.start + it.url.display.length + Math.abs(it.url.displayLengthChange) }
                                    .plus(links.map { it.url to it.start..it.start + it.url.display.length + Math.abs(it.url.displayLengthChange) })
                                    .sortedBy { it.second.start }
                                    .asReversed()
                                    .onEach { messageBuilder.replace(it.second.start, it.second.last, it.first.display) }
                                    .asReversed()

                            val display = messageBuilder.toString()
                            val offsetTree = OffsetTree(urls.map { it.second }.toTypedArray(), OffsetTree.offsets(urls.map { it.first.displayLengthChange }.toIntArray()))

                            val channelBadges = msg.rich.roomId?.let { emoteManager.getBadges(it) }

                            val globalBadges = emoteManager.getGlobalBadgeSets()
                            val badges = ArrayList<ChatListContract.Badge>()
                            val msgBadges = msg.rich.badges.clone()
                            msgBadges.forEach {
                                val msgBadge = it
                                val badge = channelBadges?.badgeSets?.get(msgBadge.name)?.versions?.get(msgBadge.version)
                                        .let { it ?: globalBadges?.badgeSets?.get(msgBadge.name)?.versions?.get(msgBadge.version) }
                                if (badge != null) {
                                    val imageUrls = arrayOf(ChatListContract.ImageUrl(badge.imageUrl1x, 1f),
                                            ChatListContract.ImageUrl(badge.imageUrl2x, 2f),
                                            ChatListContract.ImageUrl(badge.imageUrl4x, 4f))
                                    badges.add(ChatListContract.Badge(imageUrls, badge.title, badge.description))
                                }
                            }

                            val emoteSpans = emoteManager.applyEmotes(display, msg.rich.roomId)
                            val emotes = ArrayList<ChatListContract.Emote>(emoteSpans.size + (msg.rich.emotes?.size ?: 0))
                            emoteSpans.forEach {
                                val image = it.emote.images.first()
                                val starts = it.starts
                                val ends = it.ends

                                val imageUrls = image.urls.map { ChatListContract.ImageUrl(it.url, it.scale) }.toTypedArray()
                                emotes.add(ChatListContract.Emote(imageUrls, 24, 24, starts, ends))
                            }

                            val text = messageToIsAction.first
                            msg.rich.emotes?.forEach {
                                val offsets = it.starts.map { offsetTree.getOffset(it) }.toIntArray()
                                val starts = it.starts.mapIndexed { index, value -> value + offsets[index] }.toIntArray()
                                val ends = it.ends.mapIndexed { index, value -> value + offsets[index] }.toIntArray()

                                // Twitch emote positions are for grapheme clusters so we need
                                // to map it to the String representation
                                for (i in starts.indices) {
                                    val start = starts[i]
                                    val end = ends[i]
                                    val offset = start - text.codePointCount(0, start)
                                    starts[i] = start + offset
                                    ends[i] = end + offset
                                }

                                val image1x = ChatListContract.ImageUrl(HttpUrl.parse("https://static-cdn.jtvnw.net/emoticons/v1/${it.emoteId}/1.0"), 1f)
                                val image2x = ChatListContract.ImageUrl(HttpUrl.parse("https://static-cdn.jtvnw.net/emoticons/v1/${it.emoteId}/2.0"), 2f)
                                val image4x = ChatListContract.ImageUrl(HttpUrl.parse("https://static-cdn.jtvnw.net/emoticons/v1/${it.emoteId}/4.0"), 4f)
                                val imageUrls = arrayOf(image1x, image2x, image4x)
                                emotes.add(ChatListContract.Emote(imageUrls, 24, 24, starts, ends))
                            }

                            // mentions by starting position
                            val mentions = SparseArrayCompat<ChatListContract.Mention>()
                            MENTION_PATTERN.findAll(display)
                                    .forEach {
                                        if (it.value.equals("@$channel", ignoreCase = true)) {
                                            mentions.put(it.range.start, ChatListContract.Mention(true, it.range.start, it.range.endInclusive + 1))
                                        } else {
                                            mentions.put(it.range.start, ChatListContract.Mention(false, it.range.start, it.range.endInclusive + 1))
                                        }
                                    }

                            val byHost = msg.user.equals(channel, ignoreCase = true)

                            ChatListContract.Message.PrivMsg(msg.received,
                                    msg.rich.sent,
                                    ChatListContract.User(msg.user, byHost, msg.rich.color),
                                    msg.roomName,
                                    display,
                                    messageToIsAction.second,
                                    emotes.toTypedArray(),
                                    badges.toTypedArray(),
                                    mentions.values.toTypedArray(),
                                    images.map {
                                        val offset = offsetTree.getOffset(it.start)
                                        it.copy(start = it.start + offset, end = it.end + offset)
                                    }.toTypedArray(),
                                    links.map {
                                        val offset = offsetTree.getOffset(it.start)
                                        it.copy(start = it.start + offset, end = it.end + offset)
                                    }.toTypedArray())
                        }
                    }
                }
                .filter { it !is ChatListContract.Message.Invalid }
                .buffer(MESSAGE_BUFFER_MS, TimeUnit.MILLISECONDS)
                .filter { it.isNotEmpty() }
                .subscribeOn(ioScheduler)
                .observeOn(mainThreadScheduler)
                .subscribe({
                    it.reverse()
                    this.view?.addMessages(it)
                }, {
                    throw it
                }))

        // Load global badges
        subscriptions.add(twitchApi.loadGlobalBadgesBeta()
                .map<Option<BadgeSets>> { Option.Some(it) }
                .onErrorReturn { Option.None }
                .subscribeOn(ioScheduler)
                .observeOn(mainThreadScheduler)
                .subscribe({
                    val e = when (it) {
                        Option.None -> Unit
                        is Option.Some -> emoteManager.setGlobalBadgeSets(it.value)
                    }
                }, {
                    throw it
                }))

        // Load badges for channel
        subscriptions.add(authenticatedConnections
                .flatMap {
                    when (it) {
                        Option.None -> Flowable.just(Option.None)
                        is Option.Some -> it.value.rooms()
                                .flatMap { Flowable.fromIterable(it.toList()) }
                                .flatMap {
                                    val room = it
                                    twitchApi.loadChannelBadgesBeta(TwitchApi.CHANNEL_BADGES_FORMAT.format(room.channel.id))
                                            .subscribeOn(ioScheduler)
                                            .map<Option<Pair<Room, BadgeSets>>> { Option.Some(room to it) }
                                            .onErrorReturn { Option.None }
                                            .toFlowable()
                                }
                    }
                }
                .observeOn(mainThreadScheduler)
                .subscribe({
                    val e = when (it) {
                        Option.None -> Unit
                        is Option.Some -> emoteManager.setBadgeSets(it.value.first.channel, it.value.second)
                    }
                }, {
                    throw it
                }))

        // Load bttv emotes for channel
        subscriptions.add(authenticatedConnections
                .flatMap {
                    when (it) {
                        Option.None -> Flowable.just(Option.None)
                        is Option.Some -> it.value.rooms()
                                .flatMap { Flowable.fromIterable(it.toList()) }
                                .flatMap {
                                    val channel = it.channel
                                    bttvApi.getChannel(channel.name)
                                            .subscribeOn(ioScheduler)
                                            .map {
                                                val template = it.urlTemplate!!
                                                it.emotes?.map {
                                                    val image1x = ImageUrl(HttpUrl.parse("https:" + UrlUtils.applyToUrlTemplate(template, it.id, "1x")), 1f)
                                                    val image2x = ImageUrl(HttpUrl.parse("https:" + UrlUtils.applyToUrlTemplate(template, it.id, "2x")), 2f)
                                                    val image4x = ImageUrl(HttpUrl.parse("https:" + UrlUtils.applyToUrlTemplate(template, it.id, "3x")), 4f)
                                                    val imageUrls = arrayOf(image1x, image2x, image4x)
                                                    val image = Image(imageUrls)
                                                    Emoticon(it.code.toCharArray(), emoteManager.process(it.code), arrayOf(image))
                                                }?.toTypedArray() ?: emptyArray()
                                            }
                                            .map<Option<Pair<Channel, Array<Emoticon>>>> { Option.Some(channel to it) }
                                            .onErrorReturn { Option.None }
                                            .toFlowable()
                                }
                    }
                }
                .observeOn(mainThreadScheduler)
                .subscribe({
                    val e = when (it) {
                        Option.None -> Unit
                        is Option.Some -> emoteManager.setEmotes(it.value.first, it.value.second)
                    }
                }, {
                    throw it
                }))

        // try enable caps for every channel
        subscriptions.add(authenticatedConnections
                .flatMap {
                    when (it) {
                        Option.None -> Flowable.never()
                        is Option.Some -> it.value.chatConnection.requestCapabilities(arrayOf(Capability.Tags))
                    }
                }
                .subscribe { Log.d("Chat", "Capability $it enabled") })

        // listen for options changes
        subscriptions.add(chatPreferenceManager.preferences()
                .subscribe {
                    this.view?.setChatOptions(it)
                })
    }

    override fun detach(view: ChatListContract.View) {
        if (this.view === view) {
            subscriptions.clear()
            this.view = null
        }
    }

    override fun joinRoom(channel: Channel) {
        roomJoins.onNext(channel)
    }

    override fun partRoom(channel: Channel) {
        roomParts.onNext(channel)
    }
}

private val <E> SparseArrayCompat<E>.values: List<E>
    get() {
        val items = ArrayList<E>(size())
        (0..size() - 1).mapTo(items) { valueAt(it) }
        return items
    }

class OffsetTree(private val ranges: Array<IntRange>, private val rangeOffsets: IntArray) {

    companion object {

        fun offsets(offsets: IntArray): IntArray {
            return offsets.foldIndexed(IntArray(offsets.size)) { index, acc, value ->
                acc[index] = if (index == 0) value else acc[index - 1] + value
                acc
            }
        }

    }

    /**
     * @return returns offset or 0
     */
    fun getOffset(key: Int): Int {
        val a = ranges
        var low = 0
        var high = ranges.size - 1

        var prev = (low + high).ushr(1)
        while (low <= high) {
            val mid = (low + high).ushr(1)
            val midVal = a[mid]

            if (midVal.contains(key)) {
                val leftPos = mid - 1
                if (leftPos < 0) return 0
                return rangeOffsets[leftPos]
            } else if (midVal.start < key) {
                low = mid + 1
                if (low == prev) {
                    return rangeOffsets[mid]
                } else if (low > high) {
                    return rangeOffsets[mid]
                }
                prev = mid
            } else {
                high = mid - 1
                if (high == prev) {
                    return rangeOffsets[mid - 1]
                }
                prev = mid
            }
        }

        return 0
    }

}