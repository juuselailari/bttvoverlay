package com.ij.bttvoverlay.chat.rooms

import com.ij.bttvoverlay.MainThreadScheduler
import com.ij.bttvoverlay.Option
import com.ij.bttvoverlay.chat.ChatState
import com.ij.bttvoverlay.chat.Connection
import com.ij.bttvoverlay.chat.ConnectionManager
import com.ij.bttvoverlay.model.Channel
import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.processors.PublishProcessor
import javax.inject.Inject

/**
 * Created by ilben on 13.4.2017.
 */
class RoomPresenter @Inject constructor(private val connectionManager: ConnectionManager,
                                        @MainThreadScheduler private val mainThreadScheduler: Scheduler) {

    private var view: RoomView? = null
    private val room = PublishProcessor.create<Channel>()
    private val subscriptions = CompositeDisposable()
    private val roomParts = PublishProcessor.create<Channel>()

    @Suppress("UNCHECKED_CAST")
    fun takeView(view: RoomView) {
        this.view = view

        val authConnection = connectionManager.connection()
                .flatMap { (it as? ChatState.Connected)?.chatConnection?.authentications() ?: Flowable.just(Option.None) }

        subscriptions.add(Flowable.combineLatest(arrayOf(authConnection, room), { it[0] as Option<Connection> to it[1] as Channel })
                .flatMap {
                    val conn = it.first
                    when (conn) {
                        Option.None -> Flowable.just(Option.None)
                        is Option.Some -> conn.value.roomState(it.second)
                                .map { Option.Some(it) }
                    }
                }
                .observeOn(mainThreadScheduler)
                .subscribe {
                    val state = when (it) {
                        Option.None -> null
                        is Option.Some -> it.value
                    }
                    this.view?.setRoomState(state)
                })

        subscriptions.add(Flowable.combineLatest(arrayOf(authConnection, roomParts), { it[0] as Option<Connection> to it[1] as Channel })
                .flatMap {
                    val conn = it.first
                    when (conn) {
                        Option.None -> Flowable.never()
                        is Option.Some -> conn.value.partRoom(it.second)
                                .toFlowable<Unit>()
                    }
                }
                .subscribe())
    }

    fun dropView(view: RoomView) {
        if (this.view === view) {
            subscriptions.clear()
            this.view = null
        }
    }

    fun setRoom(channel: Channel) {
        this.room.onNext(channel)
    }

    fun partRoom(channel: Channel) {
        roomParts.onNext(channel)
    }

}