package com.ij.bttvoverlay.chat

import io.reactivex.Flowable
import io.reactivex.processors.BehaviorProcessor
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by ilben on 5.4.2017.
 */
@Singleton
class ConnectionManager @Inject constructor(){

    private val connectionChanges: BehaviorProcessor<ChatState> = BehaviorProcessor.createDefault(ChatState.Disconnected())

    fun connection(): Flowable<ChatState> {
        return connectionChanges
    }

    fun connectionChanges(): Flowable<ChatState> {
        return connectionChanges.skip(1)
    }

    fun switchConnection(state: ChatState) {
        setConnection(state)
    }

    fun clearConnection() {
        setConnection(ChatState.Disconnected())
    }

    private fun setConnection(connection: ChatState) {
        connectionChanges.onNext(connection)
    }

}