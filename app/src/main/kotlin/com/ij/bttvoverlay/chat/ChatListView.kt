package com.ij.bttvoverlay.chat

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import android.support.v4.util.SparseArrayCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.style.ForegroundColorSpan
import android.text.style.TextAppearanceSpan
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.ij.bttvoverlay.model.Message
import com.ij.bttvoverlay.widget.EmptyItemAnimator
import org.jetbrains.anko.*
import org.jetbrains.anko.collections.forEachReversedByIndex
import paperparcel.PaperParcel

/**
 * Created by ilben on 5.4.2017.
 */
open class ChatListView(context: Context) : RecyclerView(context) {

    private val messageAdapter = MessageAdapter(context, Glide.with(context))
    private val llm = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, true)

    init {
        layoutManager = llm
        adapter = messageAdapter
        this.itemAnimator = EmptyItemAnimator()
        //this.itemAnimator.addDuration = 33
        //this.itemAnimator.moveDuration = 33
        this.itemAnimator.addDuration = 0
        this.itemAnimator.moveDuration = 0
        this.itemAnimator.removeDuration = 0
    }

    fun addItems(index: Int, items: List<ChatItem>) {
        messageAdapter.addItems(index, items)
    }

    fun removeItems(startPos: Int, count: Int) {
        messageAdapter.removeItems(startPos, count)
    }

    fun setChatOptions(chatOptions: ChatOptions) {
        messageAdapter.setChatOptions(chatOptions, false)

        val first = llm.findFirstVisibleItemPosition()
        val last = llm.findLastVisibleItemPosition()

        if (first >= 0 && last >= 0) {
            messageAdapter.notifyItemRangeChanged(first, last - first)
        }
    }

    override fun onSaveInstanceState(): Parcelable {
        return SavedInstanceState(super.onSaveInstanceState(), messageAdapter.getItems(), messageAdapter.getChatOptions())
    }

    override fun onRestoreInstanceState(state: Parcelable) {
        val s = state as SavedInstanceState
        super.onRestoreInstanceState(s.superState)
        messageAdapter.addItems(0, s.items)
        messageAdapter.setChatOptions(s.options, true)
    }

    @Suppress("UNCHECKED_CAST")
    class SavedInstanceState : BaseSavedState {

        companion object {
            @JvmField val CREATOR = object : Parcelable.Creator<SavedInstanceState> {
                override fun createFromParcel(source: Parcel): SavedInstanceState {
                    return SavedInstanceState(source)
                }

                override fun newArray(size: Int): Array<SavedInstanceState?> {
                    return arrayOfNulls(size)
                }
            }
        }

        val items: List<ChatItem>
        val options: ChatOptions

        constructor(source: Parcel) : super(source) {
            this.items = source.readArrayList(null) as List<ChatItem>
            this.options = source.readParcelable(ChatOptions::class.java.classLoader)
        }

        constructor(source: Parcel, loader: ClassLoader?) : super(source, loader) {
            this.items = source.readArrayList(loader) as List<ChatItem>
            this.options = source.readParcelable(loader)
        }

        constructor(superState: Parcelable, items: List<ChatItem>, options: ChatOptions) : super(superState) {
            this.items = items
            this.options = options
        }

        override fun writeToParcel(out: Parcel, flags: Int) {
            super.writeToParcel(out, flags)

            out.writeList(items)
            out.writeParcelable(options, 0)
        }
    }
}

class MessageAdapter(val context: Context,
                     val glide: RequestManager) : RecyclerView.Adapter<VHMessage>() {

    private var chatOptions = ChatOptions.DEFAULT
    private var savedInstanceStateRestored = false

    private val ankoContext = AnkoContext.createReusable(context)
    private val items = ArrayList<ChatItem>()

    private val senderTextAppearanceSpanCache = SparseArrayCompat<TextAppearanceSpan>()
    private val foregroundColorSpanCache = SparseArrayCompat<ForegroundColorSpan>()

    init {
        setHasStableIds(true)
    }

    fun getItems() = items

    fun getChatOptions() = chatOptions

    fun setChatOptions(chatOptions: ChatOptions, fromSavedInstanceState: Boolean) {
        if (fromSavedInstanceState && savedInstanceStateRestored) return
        if (this.chatOptions == chatOptions) return

        savedInstanceStateRestored = true
        this.chatOptions = chatOptions
    }

    fun addItems(startPos: Int, items: List<ChatItem>) {
        items.forEachReversedByIndex {
            this.items.add(startPos, it)
        }
        notifyItemRangeInserted(startPos, items.size)
    }

    fun removeItems(startPos: Int, count: Int) {
        (0..count - 1).forEach {
            this.items.removeAt(startPos)
        }
        notifyItemRangeRemoved(startPos, count)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onViewRecycled(holder: VHMessage) {
        super.onViewRecycled(holder)
        Glide.clear(holder.view)
    }

    override fun onBindViewHolder(holder: VHMessage, position: Int) {
        holder.view.clearManagedDrawables()

        val item = items[position]
        val exhaust = when (item) {
            is ChatItem.Invalid -> {
                holder.view.bindInvalid(item)
            }
            is ChatItem.PrivMsg -> {
                holder.view.bindPrivMsg(chatOptions, senderTextAppearanceSpanCache, foregroundColorSpanCache, item)
            }
            is ChatItem.Connect -> {
                holder.view.bindConnect(item)
            }
            is ChatItem.Join -> {
                holder.view.bindJoin(item)
            }
            is ChatItem.Part -> {
                holder.view.bindPart(item)
            }
        }
    }

    override fun getItemId(position: Int): Long {
        return items[position].id
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHMessage {
        return VHMessage(MessageViewUI.createView(ankoContext))
    }

}

class VHMessage(val view: MessageView) : RecyclerView.ViewHolder(view)

object MessageViewUI : AnkoComponent<Context> {
    override fun createView(ui: AnkoContext<Context>): MessageView = with(ui) {
        messageView {
            layoutParams = RecyclerView.LayoutParams(matchParent, wrapContent)
            horizontalPadding = dip(12)
        }
    }
}

sealed class ChatItem(val id: Long) {
    @PaperParcel
    class Invalid(id: Long, val message: Message.Invalid) : ChatItem(id), Parcelable {
        companion object {
            @JvmField val CREATOR = PaperParcelChatItem_Invalid.CREATOR
        }

        override fun describeContents() = 0
        override fun writeToParcel(dest: Parcel, flags: Int) = PaperParcelChatItem_Invalid.writeToParcel(this, dest, flags)
    }

    @PaperParcel
    class PrivMsg(id: Long, val message: Message.PrivMsg) : ChatItem(id), Parcelable {
        companion object {
            @JvmField val CREATOR = PaperParcelChatItem_PrivMsg.CREATOR
        }

        override fun describeContents() = 0
        override fun writeToParcel(dest: Parcel, flags: Int) = PaperParcelChatItem_PrivMsg.writeToParcel(this, dest, flags)
    }

    @PaperParcel
    class Join(id: Long, val message: Message.Join) : ChatItem(id), Parcelable {
        companion object {
            @JvmField val CREATOR = PaperParcelChatItem_Join.CREATOR
        }

        override fun describeContents() = 0
        override fun writeToParcel(dest: Parcel, flags: Int) = PaperParcelChatItem_Join.writeToParcel(this, dest, flags)
    }

    @PaperParcel
    class Part(id: Long, val message: Message.Part) : ChatItem(id), Parcelable {
        companion object {
            @JvmField val CREATOR = PaperParcelChatItem_Part.CREATOR
        }

        override fun describeContents() = 0
        override fun writeToParcel(dest: Parcel, flags: Int) = PaperParcelChatItem_Part.writeToParcel(this, dest, flags)
    }

    @PaperParcel
    class Connect(id: Long, val message: Message.Connect) : ChatItem(id), Parcelable {
        companion object {
            @JvmField val CREATOR = PaperParcelChatItem_Connect.CREATOR
        }

        override fun describeContents() = 0
        override fun writeToParcel(dest: Parcel, flags: Int) = PaperParcelChatItem_Connect.writeToParcel(this, dest, flags)
    }
    //class Disconnect(id: Long, val message: Message.Connect) : ChatItem(id)
}