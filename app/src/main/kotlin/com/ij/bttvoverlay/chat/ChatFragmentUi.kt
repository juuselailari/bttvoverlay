package com.ij.bttvoverlay.chat

import android.content.Context
import android.support.design.widget.AppBarLayout
import android.view.View
import android.view.ViewGroup
import com.ij.bttvoverlay.R
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.titleResource
import org.jetbrains.anko.appcompat.v7.toolbar
import org.jetbrains.anko.design.appBarLayout
import org.jetbrains.anko.design.coordinatorLayout

/**
 * Created by ilben on 6.4.2017.
 */
object ChatFragmentUi : AnkoComponent<Context> {
    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        coordinatorLayout {
            layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)

            appBarLayout(theme = R.style.ThemeOverlay_AppTheme_ActionBar) {
                id = R.id.appBarLayout
                toolbar {
                    id = R.id.toolbar
                    titleResource = R.string.title_chat
                    inflateMenu(R.menu.chat)
                }.lparams(matchParent, wrapContent) {
                    scrollFlags = 0
                }
            }.lparams(matchParent, wrapContent)
            chatView {
                id = R.id.list
                verticalPadding = dip(12)
                clipToPadding = false
                setHasFixedSize(true)
            }.lparams(matchParent, matchParent) {
                behavior = AppBarLayout.ScrollingViewBehavior()
            }
        }
    }


}