package com.ij.bttvoverlay.chat.badge

import android.content.Context
import android.support.v4.view.GravityCompat
import android.support.v7.widget.LinearLayoutCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.ViewManager
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.ij.bttvoverlay.R
import com.ij.bttvoverlay.glide.RoundedCornerBitmapTransformation
import com.ij.bttvoverlay.model.Message
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.linearLayoutCompat
import org.jetbrains.anko.appcompat.v7.tintedTextView
import org.jetbrains.anko.custom.ankoView

/**
 * Created by ilben on 24.4.2017.
 */
class BadgeListView(context: Context) : RecyclerView(context) {

    private val badgeAdapter = Adapter(context)

    init {
        layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        adapter = badgeAdapter
    }

    fun setBadges(badges: Array<Message.Badge>) {
        badgeAdapter.setBadges(badges)
    }

    class Adapter(val context: Context) : RecyclerView.Adapter<BadgeVH>() {

        private val ankoContext = AnkoContext.createReusable(context)
        private val badges = ArrayList<Message.Badge>()

        fun setBadges(badges: Array<Message.Badge>) {
            this.badges.clear()
            this.badges.addAll(badges)
            notifyDataSetChanged()
        }

        override fun onBindViewHolder(holder: BadgeVH, position: Int) {
            val badge = badges[position]
            holder.view.bind(badge)
        }

        override fun getItemCount(): Int = badges.size

        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): BadgeVH = BadgeVH(BadgeViewUi.createView(ankoContext))

    }

    class BadgeVH(val view: BadgeView) : ViewHolder(view)
}

class BadgeView(context: Context) : FrameLayout(context) {

    companion object {
        val ANKO_FACTORY = { ctx: Context -> BadgeView(ctx) }
    }

    var badge: Message.Badge? = null
        private set

    private lateinit var ivIcon: ImageView
    private lateinit var tvTitle: TextView
    private lateinit var tvDescription: TextView

    private val badgeTransform = RoundedCornerBitmapTransformation(context,
            cornerRadius = resources.getDimension(R.dimen.message_badge_corners),
            isCircular = false)

    init {
        with(this) {
            ivIcon = imageView {
                layoutParams = FrameLayout.LayoutParams(dip(48), dip(48)).apply {
                    gravity = GravityCompat.START
                    margin = dip(16)
                }
            }

            linearLayoutCompat {
                orientation = LinearLayoutCompat.VERTICAL
                gravity = Gravity.CENTER_VERTICAL

                layoutParams = FrameLayout.LayoutParams(matchParent, wrapContent).apply {
                    gravity = Gravity.CENTER_VERTICAL
                    marginStart = dip(80)
                }
                tvTitle = tintedTextView {
                    setTextAppearance(R.style.TextAppearance_AppCompat_Body1)
                    layoutParams = FrameLayout.LayoutParams(matchParent, wrapContent)
                }

                tvDescription = tintedTextView {
                    setTextAppearance(R.style.TextAppearance_AppCompat_Caption)
                    layoutParams = FrameLayout.LayoutParams(matchParent, wrapContent)
                }
            }
        }
    }

    fun bind(badge: Message.Badge) {
        this.badge = badge

        // Load the badge with the highest available quality
        Glide.with(context)
                .load(badge.urls.last().url.toString())
                .transform(badgeTransform)
                .into(ivIcon)

        tvTitle.text = badge.title

        if (badge.description.isNotBlank()) {
            tvDescription.visibility = View.VISIBLE
            tvDescription.text = badge.description
        } else {
            tvDescription.visibility = View.GONE
        }
    }

}

inline fun ViewManager.badgeView(theme: Int = 0, init: BadgeView.() -> Unit): BadgeView {
    return ankoView(BadgeView.ANKO_FACTORY, theme) { init() }
}

object BadgeViewUi : AnkoComponent<Context> {
    override fun createView(ui: AnkoContext<Context>): BadgeView = with(ui) {
        badgeView {
            layoutParams = RecyclerView.LayoutParams(matchParent, wrapContent)
        }
    }

}