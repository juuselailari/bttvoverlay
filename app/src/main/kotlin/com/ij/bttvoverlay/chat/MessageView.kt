package com.ij.bttvoverlay.chat

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.support.v4.util.SparseArrayCompat
import android.text.Spannable
import android.text.Spanned
import android.text.format.DateFormat
import android.text.method.LinkMovementMethod
import android.text.style.ForegroundColorSpan
import android.text.style.TextAppearanceSpan
import android.view.ViewManager
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.ij.bttvoverlay.R
import com.ij.bttvoverlay.chat.MessageView.Companion.ANKO_FACTORY
import com.ij.bttvoverlay.chat.span.CustomTabsURLSpan
import com.ij.bttvoverlay.chat.span.ImageLoadingSpan
import com.ij.bttvoverlay.chat.span.MessageInfoSpan
import com.ij.bttvoverlay.glide.ImageSpanTarget
import com.ij.bttvoverlay.glide.RoundedCornerBitmapTransformation
import com.ij.bttvoverlay.model.Message
import org.jetbrains.anko.append
import org.jetbrains.anko.buildSpanned
import org.jetbrains.anko.custom.ankoView
import org.jetbrains.anko.dip
import org.jetbrains.anko.verticalPadding
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by ilben on 5.4.2017.
 */
class MessageView(context: Context) : TextView(context) {

    companion object {
        val ANKO_FACTORY = { ctx: Context -> MessageView(ctx) }
    }

    private val managedDrawables = ArrayList<GlideDrawable>()
    private var animateEmotes = true

    private val glide: RequestManager = Glide.with(context)
    private val badgeBitmapTransform = RoundedCornerBitmapTransformation(context, context.resources.getDimension(R.dimen.message_badge_corners), false)

    private val timestampFormat = SimpleDateFormat(DateFormat.getBestDateTimePattern(Locale.getDefault(), "jmmss"), Locale.getDefault())

    private val senderTextAppearanceSpan: TextAppearanceSpan
    private val mentionTextAppearanceSpan: TextAppearanceSpan
    private val mentionHostTextAppearanceSpan: TextAppearanceSpan
    private val messageTimestampTextAppearanceSpan: TextAppearanceSpan
    private val messageRoomNameTextAppearanceSpan: TextAppearanceSpan
    private val roomNameTextAppearanceSpan: TextAppearanceSpan
    private val messageBackground: Drawable?
    private val messageBackgroundHost: Drawable?

    init {
        val a = context.obtainStyledAttributes(null, R.styleable.ChatMessage, R.attr.chatMessageStyle, R.style.ChatMessage)
        senderTextAppearanceSpan = TextAppearanceSpan(context, a.getResourceId(R.styleable.ChatMessage_senderTextAppearance, R.style.TextAppearance_AppTheme_Message_Sender))
        mentionTextAppearanceSpan = TextAppearanceSpan(context, a.getResourceId(R.styleable.ChatMessage_mentionTextAppearance, R.style.TextAppearance_AppTheme_Message_Mention))
        mentionHostTextAppearanceSpan = TextAppearanceSpan(context, a.getResourceId(R.styleable.ChatMessage_mentionTextAppearance, R.style.TextAppearance_AppTheme_Message_Mention))
        messageTimestampTextAppearanceSpan = TextAppearanceSpan(context, a.getResourceId(R.styleable.ChatMessage_messageTimestampTextAppearance, R.style.TextAppearance_AppTheme_Message_MessageTimestamp))
        messageRoomNameTextAppearanceSpan = TextAppearanceSpan(context, a.getResourceId(R.styleable.ChatMessage_messageRoomNameTextAppearance, R.style.TextAppearance_AppTheme_Message_MessageRoomName))
        roomNameTextAppearanceSpan = TextAppearanceSpan(context, a.getResourceId(R.styleable.ChatMessage_roomNameTextAppearance, R.style.TextAppearance_AppTheme_Message_RoomName))
        messageBackground = a.getDrawable(R.styleable.ChatMessage_messageBackground)
        messageBackgroundHost = a.getDrawable(R.styleable.ChatMessage_messageBackgroundHost)
        a.recycle()

        movementMethod = LinkMovementMethod()

        setTextAppearance(R.style.TextAppearance_AppCompat_Body1)
        verticalPadding = dip(2)
    }

    override fun verifyDrawable(who: Drawable?): Boolean {
        return managedDrawables.contains(who)
    }

    fun setAnimateEmotes(animate: Boolean) {
        this.animateEmotes = animate
    }

    fun addManagedDrawable(drawable: GlideDrawable) {
        this.managedDrawables.add(drawable)
        drawable.callback = this
    }

    fun clearManagedDrawables() {
        managedDrawables.forEach { it.callback = null }
        managedDrawables.clear()
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        if (animateEmotes) managedDrawables.forEach { it.start() }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()

        if (animateEmotes) managedDrawables.forEach { it.stop() }
    }

    private fun getSenderSpan(cache: SparseArrayCompat<TextAppearanceSpan>, color: Int): TextAppearanceSpan {
        val cached = cache.get(color)
        if (cached != null) return cached

        val newSpan = TextAppearanceSpan(senderTextAppearanceSpan.family,
                senderTextAppearanceSpan.textStyle,
                senderTextAppearanceSpan.textSize,
                ColorStateList.valueOf(color),
                senderTextAppearanceSpan.linkTextColor)
        cache.put(color, newSpan)
        return newSpan
    }

    private fun getForegroundColorSpan(cache: SparseArrayCompat<ForegroundColorSpan>, color: Int): ForegroundColorSpan {
        val cached = cache.get(color)
        if (cached != null) return cached

        val newSpan = ForegroundColorSpan(color)
        cache.put(color, newSpan)
        return newSpan
    }

    private fun pickImageUrl(chatOptions: ChatOptions, urls: Array<Message.ImageUrl>): Message.ImageUrl {
        return when (chatOptions.imageQuality) {
            ChatOptions.ImageQuality.High -> urls.last()
            ChatOptions.ImageQuality.Medium -> urls[(urls.size / 2)]
            ChatOptions.ImageQuality.Low -> urls.first()
        }
    }

    fun bindInvalid(item: ChatItem.Invalid) {
        val msg = item.message
        Glide.clear(this)
        this.text = msg.line
    }

    fun bindPrivMsg(chatOptions: ChatOptions,
                    senderTextAppearanceSpanCache: SparseArrayCompat<TextAppearanceSpan>,
                    foregroundColorSpanCache: SparseArrayCompat<ForegroundColorSpan>,
                    item: ChatItem.PrivMsg) {
        val msg = item.message
        val d = context.resources.displayMetrics.density.toInt()

        if (msg.user.host) {
            this.background = messageBackgroundHost
        } else {
            this.background = messageBackground
        }

        val display = buildSpanned {

            if (chatOptions.timestampVisible) {
                val displayDate = msg.sent ?: msg.received
                val dateDisplay = timestampFormat.format(displayDate)
                append(dateDisplay, messageTimestampTextAppearanceSpan)
                append(" ")
            }

            if (chatOptions.roomNameVisible) {
                append(msg.roomName, messageRoomNameTextAppearanceSpan)
                append(" ")
            }

            val senderInfoStart = this.length

            if (chatOptions.showBadges) {
                msg.badges.forEach {
                    val start = this.length
                    append(". ")
                    val end = this.length - 1 // -1 to match only the "."

                    val span = ImageLoadingSpan(18 * d, 18 * d)
                    setSpan(span, start, end, Spannable.SPAN_INCLUSIVE_INCLUSIVE)

                    val imageUrl = pickImageUrl(chatOptions, it.urls)
                    glide.load(imageUrl.url.toString())
                            .dontAnimate()
                            .skipMemoryCache(true) // skip memory-cache to avoid weird rendering issues
                            .transform(badgeBitmapTransform)
                            .into(ImageSpanTarget(this@MessageView, listOf(span), false, imageUrl.scale))
                }
            }

            val messageStartOffset: Int
            if (msg.isAction) {
                val senderSpan: TextAppearanceSpan
                val actionForegroundColorSpan: ForegroundColorSpan?
                if (msg.user.color != null) {
                    senderSpan = getSenderSpan(senderTextAppearanceSpanCache, msg.user.color)
                    actionForegroundColorSpan = getForegroundColorSpan(foregroundColorSpanCache, msg.user.color)
                } else {
                    senderSpan = senderTextAppearanceSpan
                    actionForegroundColorSpan = null
                }
                val actionTextStart = this.length
                append(msg.user.name, senderSpan)
                setSpan(MessageInfoSpan(msg), senderInfoStart, this.length, Spanned.SPAN_INCLUSIVE_EXCLUSIVE)
                append(" ")

                // Append message
                messageStartOffset = this.length
                append(msg.message)

                // Make the sender + message part be the sender-color
                if (actionForegroundColorSpan != null) setSpan(actionForegroundColorSpan, actionTextStart, this.length, Spanned.SPAN_INCLUSIVE_EXCLUSIVE)
            } else {
                val senderSpan: TextAppearanceSpan
                if (msg.user.color != null) {
                    senderSpan = getSenderSpan(senderTextAppearanceSpanCache, msg.user.color)
                } else {
                    senderSpan = senderTextAppearanceSpan
                }
                append(msg.user.name, senderSpan)
                setSpan(MessageInfoSpan(msg), senderInfoStart, this.length, Spanned.SPAN_INCLUSIVE_EXCLUSIVE)
                append(":  ")

                // Append message
                messageStartOffset = this.length
                append(msg.message)
            }

            // Apply emotes
            if (chatOptions.showEmotes) {
                msg.emotes.forEach {
                    val spans = it.starts.mapIndexed { index, start ->
                        val end = it.ends[index]
                        val span = ImageLoadingSpan(it.width * d, it.height * d)
                        setSpan(span, messageStartOffset + start, messageStartOffset + end + 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
                        span
                    }

                    val imageUrl = pickImageUrl(chatOptions, it.urls)
                    glide.load(imageUrl.url.toString())
                            .dontAnimate()
                            .centerCrop()
                            .skipMemoryCache(true) // skip memory-cache to avoid weird rendering issues
                            .into(ImageSpanTarget(this@MessageView, spans, chatOptions.showAnimatedEmotes, imageUrl.scale))
                }
            }

            // Apply mentions
            msg.mentions.forEach {
                val span = if (it.host) mentionHostTextAppearanceSpan else mentionTextAppearanceSpan
                setSpan(span, messageStartOffset + it.start, messageStartOffset + it.end, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            }

            // Apply any image URLs
            msg.images.forEach {
                val start = messageStartOffset + it.start
                val end = messageStartOffset + it.end
                setSpan(CustomTabsURLSpan(it.url.url), start, end, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            }

            // Apply generic links
            msg.links.forEach {
                val start = messageStartOffset + it.start
                val end = messageStartOffset + it.end
                setSpan(CustomTabsURLSpan(it.url.url), start, end, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            }
        }

        setAnimateEmotes(chatOptions.showAnimatedEmotes)
        this.text = display
    }

    fun bindConnect(item: ChatItem.Connect) {
        Glide.clear(this)
        this.text = context.getString(R.string.chat_connected)
    }

    fun bindJoin(item: ChatItem.Join) {
        val msg = item.message
        Glide.clear(this)
        val text = context.getString(R.string.chat_joined_room, msg.name)
        val display = buildSpanned {
            append(text)

            val pos = text.indexOf(msg.name)
            if (pos >= 0) {
                setSpan(roomNameTextAppearanceSpan, pos, pos + msg.name.length, Spanned.SPAN_INCLUSIVE_EXCLUSIVE)
            }
        }
        this.text = display
    }

    fun bindPart(item: ChatItem.Part) {
        val msg = item.message
        Glide.clear(this)
        val text = context.getString(R.string.chat_left_room, msg.name)
        val display = buildSpanned {
            append(text)

            val pos = text.indexOf(msg.name)
            if (pos >= 0) {
                setSpan(roomNameTextAppearanceSpan, pos, pos + msg.name.length, Spanned.SPAN_INCLUSIVE_EXCLUSIVE)
            }
        }
        this.text = display
    }

}

inline fun ViewManager.messageView(theme: Int = 0, init: MessageView.() -> Unit): MessageView {
    return ankoView(ANKO_FACTORY, theme) { init() }
}