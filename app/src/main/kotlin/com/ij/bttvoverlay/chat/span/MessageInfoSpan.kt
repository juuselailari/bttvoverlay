package com.ij.bttvoverlay.chat.span

import android.content.Context
import android.content.ContextWrapper
import android.support.v4.app.FragmentActivity
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.view.View
import com.ij.bttvoverlay.chat.MessageInfoFragment
import com.ij.bttvoverlay.model.Message

/**
 * Clickable span which shows information of the [message]
 *
 * Created by ilben on 12.4.2017.
 */
class MessageInfoSpan(val message: Message.PrivMsg) : ClickableSpan() {
    override fun onClick(widget: View) {
        val activity = getActivity(widget.context)
        val fm = activity.supportFragmentManager
        val fragment = MessageInfoFragment.create(message)
        fragment.show(fm, MessageInfoFragment.TAG)
    }

    override fun updateDrawState(ds: TextPaint?) = Unit

    private fun getActivity(context: Context): FragmentActivity {
        if (context is FragmentActivity) return context
        return getActivity((context as ContextWrapper).baseContext)
    }


}