package com.ij.bttvoverlay.chat.rooms

import android.content.Context
import android.support.v4.view.GravityCompat
import android.support.v7.widget.LinearLayoutCompat.VERTICAL
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.ij.bttvoverlay.ATTRS_SELECTABLE_ITEM_BACKGROUND_BORDERLESS
import com.ij.bttvoverlay.Option
import com.ij.bttvoverlay.R
import com.ij.bttvoverlay.chat.RoomState
import com.ij.bttvoverlay.dagger.ViewInjections
import com.ij.bttvoverlay.dagger.ViewKey
import com.ij.bttvoverlay.getAttributeDrawable
import com.ij.bttvoverlay.glide.RoundedCornerBitmapTransformation
import com.ij.bttvoverlay.model.Channel
import com.jakewharton.rxbinding2.view.clicks
import dagger.Binds
import dagger.android.AndroidInjector
import dagger.multibindings.IntoMap
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.processors.BehaviorProcessor
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.linearLayoutCompat
import org.jetbrains.anko.appcompat.v7.tintedTextView
import javax.inject.Inject

/**
 * Created by ilben on 13.4.2017.
 */
class RoomView(context: Context) : FrameLayout(context) {

    @dagger.Subcomponent(modules = arrayOf())
    interface Subcomponent : AndroidInjector<RoomView> {
        @dagger.Subcomponent.Builder
        abstract class Builder : AndroidInjector.Builder<RoomView>()
    }

    @dagger.Module(subcomponents = arrayOf(Subcomponent::class))
    abstract class Module {
        @Binds
        @IntoMap
        @ViewKey(RoomView::class)
        abstract fun bindInjectorFactory(builder: Subcomponent.Builder): AndroidInjector.Factory<out View>
    }

    @dagger.Component(modules = arrayOf(Module::class))
    interface Component

    @Inject
    lateinit var presenter: RoomPresenter

    private lateinit var ivChannelIcon: ImageView
    private lateinit var tvRoomName: TextView
    private lateinit var tvRoomState: TextView
    private lateinit var ibRemove: ImageButton

    private val channel = BehaviorProcessor.createDefault<Option<Channel>>(Option.None)
    private val subscriptions = CompositeDisposable()

    init {
        ViewInjections.inject(this)
        with(this) {

            ivChannelIcon = imageView {
                layoutParams = FrameLayout.LayoutParams(dip(48), dip(48)).apply {
                    gravity = GravityCompat.START
                    margin = dip(16)
                }
            }

            linearLayoutCompat {
                orientation = VERTICAL
                gravity = Gravity.CENTER_VERTICAL

                layoutParams = FrameLayout.LayoutParams(matchParent, wrapContent).apply {
                    gravity = Gravity.CENTER_VERTICAL
                    marginStart = dip(80)
                }
                tvRoomName = tintedTextView {
                    setTextAppearance(R.style.TextAppearance_AppCompat_Subhead)
                    layoutParams = FrameLayout.LayoutParams(matchParent, wrapContent)
                }

                tvRoomState = tintedTextView {
                    setTextAppearance(R.style.TextAppearance_AppCompat_Body2)
                    layoutParams = FrameLayout.LayoutParams(matchParent, wrapContent)
                }
            }

            ibRemove = imageButton {
                layoutParams = FrameLayout.LayoutParams(dip(56), dip(56)).apply {
                    gravity = GravityCompat.END or Gravity.CENTER_VERTICAL
                    marginEnd = dip(16)
                }
                background = context.getAttributeDrawable(ATTRS_SELECTABLE_ITEM_BACKGROUND_BORDERLESS)
                imageResource = R.drawable.ic_clear_control_24dp
            }
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        presenter.takeView(this)

        subscriptions.add(channel.subscribe {
            if (it is Option.Some) {
                tvRoomName.text = it.value.name
                tvRoomState.text = null
                presenter.setRoom(it.value)
                Glide.with(context)
                        .load(it.value.icon)
                        .transform(RoundedCornerBitmapTransformation(context, isCircular = true))
                        .crossFade()
                        .into(ivChannelIcon)
            }
        })

        subscriptions.add(Flowable.combineLatest(arrayOf(channel, ibRemove.clicks().toFlowable(BackpressureStrategy.LATEST)), { it[0] as Option<Channel> })
                .subscribe {
                    if (it is Option.Some) {
                        presenter.partRoom(it.value)
                    }
                })
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()

        subscriptions.clear()
        presenter.dropView(this)
    }

    fun setRoom(channel: Channel) {
        this.channel.onNext(Option.Some(channel))
    }

    fun setRoomState(roomState: RoomState?) {
        val textRes = when (roomState) {
            RoomState.CONNECTING -> R.string.chat_room_state_connecting
            RoomState.CONNECTED -> R.string.chat_room_state_connected
            RoomState.DISCONNECTING -> R.string.chat_room_state_disconnecting
            RoomState.DISCONNECTED -> R.string.chat_room_state_disconnected
            null -> -1
        }
        tvRoomState.textResource = textRes
    }

}