package com.ij.bttvoverlay.chat.span

import android.graphics.Color
import android.net.Uri
import android.support.customtabs.CustomTabsIntent
import android.text.style.ClickableSpan
import android.view.View
import com.ij.bttvoverlay.ATTRS_COLOR_PRIMARY
import com.ij.bttvoverlay.R
import com.ij.bttvoverlay.getAttributeColor
import okhttp3.HttpUrl

/**
 * Clickable Span which opens the [url] with CustomTabs
 *
 * Created by ilben on 12.4.2017.
 */
class CustomTabsURLSpan(val url: HttpUrl) : ClickableSpan() {

    override fun onClick(widget: View) {
        val ctx = widget.context
        CustomTabsIntent.Builder()
                .setToolbarColor(ctx.getAttributeColor(ATTRS_COLOR_PRIMARY, Color.WHITE))
                .setStartAnimations(ctx, R.anim.custom_tabs_browser_enter, R.anim.custom_tabs_app_exit)
                .setExitAnimations(ctx, R.anim.custom_tabs_app_enter, R.anim.custom_tabs_browser_exit)
                .build()
                .launchUrl(widget.context, Uri.parse(url.toString()))
    }


}