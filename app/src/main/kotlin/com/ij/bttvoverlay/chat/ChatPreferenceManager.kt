package com.ij.bttvoverlay.chat

import android.content.Context
import android.content.SharedPreferences
import com.ij.bttvoverlay.AppContext
import com.ij.bttvoverlay.DefaultPreferences
import com.ij.bttvoverlay.R
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by ilben on 20.4.2017.
 */
@Singleton
class ChatPreferenceManager @Inject constructor(
        @AppContext val context: Context,
        @DefaultPreferences val preferences: SharedPreferences) {

    companion object {
        const val DEFAULT_IMAGE_QUALITY = "low"
    }

    private val keyTimestampVisible = context.getString(R.string.pref_show_timestamp)
    private val keyRoomNameVisible = context.getString(R.string.pref_show_room_name)
    private val keyShowBadges = context.getString(R.string.pref_show_badges)
    private val keyEmotesEnabled = context.getString(R.string.pref_show_emotes)
    private val keyAnimatedEmotesEnabled = context.getString(R.string.pref_show_animated_emotes)
    private val keyImageQuality = context.getString(R.string.pref_image_quality)

    fun preferences(): Observable<ChatOptions> {
        return Observable.create<ChatOptions> { emitter ->
            val callback = SharedPreferences.OnSharedPreferenceChangeListener { _, _ -> emitter.onNext(getCurrent()) }
            preferences.registerOnSharedPreferenceChangeListener(callback)
            emitter.setCancellable { preferences.unregisterOnSharedPreferenceChangeListener(callback) }
        }.startWith(Observable.fromCallable { getCurrent() })
    }

    private fun getCurrent() = ChatOptions(preferences.getBoolean(keyTimestampVisible, false),
            preferences.getBoolean(keyRoomNameVisible, false),
            preferences.getBoolean(keyShowBadges, false),
            preferences.getBoolean(keyEmotesEnabled, true),
            preferences.getBoolean(keyAnimatedEmotesEnabled, true),
            ChatOptions.ImageQuality.parseQuality(preferences.getString(keyImageQuality, DEFAULT_IMAGE_QUALITY)))

}