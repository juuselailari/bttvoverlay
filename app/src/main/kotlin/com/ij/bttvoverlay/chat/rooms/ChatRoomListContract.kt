package com.ij.bttvoverlay.chat.rooms

import com.ij.bttvoverlay.model.Channel

/**
 * Created by ilben on 13.4.2017.
 */
interface ChatRoomListContract {

    interface View {
        fun setRooms(rooms: Array<Channel>)
    }

    interface Presenter {
        fun takeView(view: View)
        fun dropView(view: View)
    }

}