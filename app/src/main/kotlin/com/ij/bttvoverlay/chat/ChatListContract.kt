package com.ij.bttvoverlay.chat

import com.ij.bttvoverlay.model.Channel
import okhttp3.HttpUrl
import java.util.*

/**
 * Created by ilben on 5.4.2017.
 */
interface ChatListContract {
    interface View {

        fun setRooms(rooms: Array<Channel>)
        fun addMessages(messages: List<Message>)
        fun setChatOptions(chatOptions: ChatOptions)

    }

    interface Presenter {

        fun attach(view: View)
        fun detach(view: View)

        fun joinRoom(channel: Channel)
        fun partRoom(channel: Channel)

    }

    sealed class Message {

        data class Invalid(val line: String) : Message()

        data class PrivMsg(val received: Date,
                           val sent: Date?,
                           val user: User,
                           val roomName: String,
                           val message: String,
                           val isAction: Boolean,
                           val emotes: Array<Emote>,
                           val badges: Array<Badge>,
                           val mentions: Array<Mention>,
                           val images: Array<Image>,
                           val links: Array<Link>) : Message()

        class Connect : Message()

        data class Join(val name: String) : Message()

        data class Part(val name: String) : Message()
    }

    data class User(val name: String,
                    val host: Boolean,
                    val color: Int?)

    data class Emote(
            /** All versions of the emote url where the lowest quality one is first*/
            val urls: Array<ImageUrl>,
            val width: Int,
            val height: Int,
            val starts: IntArray,
            /**Inclusive end positions*/
            val ends: IntArray)

    data class Badge(
            /** All versions of the badge url where the lowest quality one is first*/
            val urls: Array<ImageUrl>,
            val title: String, val description: String)

    data class Mention(val host: Boolean,
                       val start: Int,
                       val end: Int)

    data class Image(val url: Url,
                     val start: Int,
                     val end: Int)

    data class ImageUrl(val url: HttpUrl,
                        val scale: Float)

    data class Link(val url: Url,
                    val start: Int,
                    val end: Int)

    data class Url(val url: HttpUrl,
                   val display: String,
                   val displayLengthChange: Int)

}