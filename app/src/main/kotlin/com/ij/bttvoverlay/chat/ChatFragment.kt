package com.ij.bttvoverlay.chat

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.transition.TransitionManager
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.ButterFragment
import butterknife.bindView
import com.ij.bttvoverlay.ButterDaggerFragment
import com.ij.bttvoverlay.R
import com.ij.bttvoverlay.auth.FinishAuthFragment
import com.ij.bttvoverlay.chat.rooms.ChatRoomListFragment
import com.ij.bttvoverlay.model.Channel
import com.jakewharton.rxbinding2.support.v7.widget.itemClicks
import dagger.Binds
import dagger.android.AndroidInjector
import dagger.android.support.FragmentKey
import dagger.multibindings.IntoMap
import io.reactivex.disposables.CompositeDisposable
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.support.v4.withArguments
import javax.inject.Inject

/**
 * Created by ilben on 6.4.2017.
 */
class ChatFragment : ButterDaggerFragment(), ChatListContract.View {

    @dagger.Subcomponent(modules = arrayOf())
    interface Subcomponent : AndroidInjector<ChatFragment> {
        @dagger.Subcomponent.Builder
        abstract class Builder : AndroidInjector.Builder<ChatFragment>()
    }

    @dagger.Module(subcomponents = arrayOf(Subcomponent::class))
    abstract class Module {
        @Binds
        @IntoMap
        @FragmentKey(ChatFragment::class)
        abstract fun bindInjectorFactory(builder: Subcomponent.Builder): AndroidInjector.Factory<out Fragment>
    }

    @dagger.Component(modules = arrayOf(Module::class))
    interface Component

    companion object {

        const val TAG = "chat_fragment"

        private const val TAG_CONNECTION_MANAGER = "frag:conn_manager"
        private const val ARG_CHANNEL = "arg:channel"

        fun create(channel: Channel): ChatFragment {
            return ChatFragment()
                    .withArguments(ARG_CHANNEL to channel)
        }

    }

    lateinit var channel: Channel

    private val subscriptions = CompositeDisposable()

    private val appBarLayout by bindView<AppBarLayout>(R.id.appBarLayout)
    private val toolbar by bindView<Toolbar>(R.id.toolbar)
    private val chatView by bindView<ChatView>(R.id.list)

    private var chatRooms: List<Channel> = emptyList()

    @Inject
    lateinit var presenter: ChatPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        channel = arguments.getParcelable<Channel>(ARG_CHANNEL)

        context.startService(Intent(context, ConnectionService::class.java))
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter.attach(this)
    }

    private fun onRoomsChanged() {
        TransitionManager.beginDelayedTransition(toolbar)
        toolbar.subtitle = this.chatRooms.joinToString(separator = ", ") { it.name }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        // Add connectionManager as a child fragment so it will
        // be correctly disposed as this fragment is popped from stack
        if (childFragmentManager.findFragmentByTag(TAG_CONNECTION_MANAGER) == null) {
            childFragmentManager.beginTransaction()
                    .add(ConnectionFragment(), TAG_CONNECTION_MANAGER)
                    .commit()

            if (savedInstanceState == null) {
                presenter.joinRoom(channel)
            }
        }
    }

    override fun onResume() {
        super.onResume()

        subscriptions.add(toolbar.itemClicks()
                .subscribe {
                    when (it.itemId) {
                        R.id.action_settings -> {

                            fragmentManager.beginTransaction()
                                    .replace(R.id.container, ChatSettingsFragment())
                                    .addToBackStack(null)
                                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                                    .commit()
                        }
                        R.id.action_chat_room_list -> {
                            fragmentManager.beginTransaction()
                                    .add(R.id.container, ChatRoomListFragment())
                                    .addToBackStack(null)
                                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                                    .commit()
                        }
                    }
                })
    }

    override fun onStop() {
        super.onStop()

        subscriptions.clear()
    }

    override fun onDestroyView() {
        super.onDestroyView()

        presenter.detach(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return ChatFragmentUi.createView(AnkoContext.create(container!!.context))
    }

    override fun setRooms(rooms: Array<Channel>) {
        this.chatRooms = rooms.toList()
        onRoomsChanged()
    }

    override fun addMessages(messages: List<ChatListContract.Message>) {
        chatView.addMessages(messages)
    }

    override fun setChatOptions(chatOptions: ChatOptions) {
        chatView.setChatOptions(chatOptions)
    }
}