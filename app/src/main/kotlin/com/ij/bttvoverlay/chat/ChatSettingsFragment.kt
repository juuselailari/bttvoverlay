package com.ij.bttvoverlay.chat

import android.content.Context
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.v7.preference.PreferenceFragmentCompat
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.ButterFragment
import butterknife.bindView
import com.ij.bttvoverlay.R
import com.jakewharton.rxbinding2.support.v7.widget.navigationClicks
import io.reactivex.disposables.CompositeDisposable
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.navigationIconResource
import org.jetbrains.anko.appcompat.v7.titleResource
import org.jetbrains.anko.appcompat.v7.toolbar
import org.jetbrains.anko.design.appBarLayout
import org.jetbrains.anko.design.coordinatorLayout

/**
 * Created by ilben on 20.4.2017.
 */
class ChatSettingsFragment : ButterFragment() {

    companion object {
        private const val PREFERENCES_TAG = "pref:tag"
    }

    private val toolbar by bindView<Toolbar>(R.id.toolbar)
    private val subscriptions = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return ChatSettingsFragmentUi.createView(AnkoContext.create(container!!.context))
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        subscriptions.add(toolbar.navigationClicks().subscribe { fragmentManager.popBackStack() })
    }

    override fun onDestroyView() {
        super.onDestroyView()

        subscriptions.clear()
    }

    override fun onResume() {
        super.onResume()

        val fm = childFragmentManager
        if (fm.findFragmentByTag(PREFERENCES_TAG) == null) {
            fm.beginTransaction()
                    .replace(R.id.container, ChatPreferences(), PREFERENCES_TAG)
                    .commit()
        }
    }

    class ChatPreferences : PreferenceFragmentCompat() {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            addPreferencesFromResource(R.xml.settings)
        }
    }
}

object ChatSettingsFragmentUi : AnkoComponent<Context> {

    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        coordinatorLayout {
            layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)
            appBarLayout(theme = R.style.ThemeOverlay_AppTheme_ActionBar) {
                toolbar {
                    id = R.id.toolbar
                    titleResource = R.string.item_settings
                    navigationIconResource = R.drawable.abc_ic_ab_back_material
                }.lparams(matchParent, wrapContent) {
                    scrollFlags = 0
                }
            }.lparams(matchParent, wrapContent)

            frameLayout {
                id = R.id.container
            }.lparams(matchParent, matchParent) {
                behavior = AppBarLayout.ScrollingViewBehavior()
            }
        }
    }
}