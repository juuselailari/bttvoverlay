package com.ij.bttvoverlay.chat

import android.os.Parcel
import android.os.Parcelable
import paperparcel.PaperParcel
import java.util.*

/**
 * Created by ilben on 20.4.2017.
 */
@PaperParcel
data class ChatOptions(val timestampVisible: Boolean,
                       val roomNameVisible: Boolean,
                       val showBadges: Boolean,
                       val showEmotes: Boolean,
                       val showAnimatedEmotes: Boolean,
                       val imageQuality: ImageQuality) : Parcelable {

    companion object {
        val DEFAULT = ChatOptions(false, false, true, true, true, ImageQuality.Low)
        @JvmField val CREATOR = PaperParcelChatOptions.CREATOR
    }

    override fun writeToParcel(dest: Parcel, flags: Int) = PaperParcelChatOptions.writeToParcel(this, dest, flags)

    override fun describeContents(): Int = 0

    enum class ImageQuality {
        High,
        Medium,
        Low;

        companion object {

            fun parseQuality(name: String): ImageQuality {
                return when (name.toLowerCase(Locale.US)) {
                    "high" -> ImageQuality.High
                    "medium" -> ImageQuality.Medium
                    else -> ImageQuality.Low
                }
            }
        }
    }

}