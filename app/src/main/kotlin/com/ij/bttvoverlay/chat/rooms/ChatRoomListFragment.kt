package com.ij.bttvoverlay.chat.rooms

import android.content.Context
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v4.view.GravityCompat
import android.support.v7.widget.Toolbar
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.ButterFragment
import butterknife.bindView
import com.ij.bttvoverlay.*
import com.ij.bttvoverlay.chat.ChatState
import com.ij.bttvoverlay.chat.Connection
import com.ij.bttvoverlay.chat.ConnectionManager
import com.ij.bttvoverlay.model.Channel
import com.ij.bttvoverlay.search.ChannelSearchListener
import com.ij.bttvoverlay.search.SearchFragment
import com.jakewharton.rxbinding2.support.v7.widget.navigationClicks
import com.jakewharton.rxbinding2.view.clicks
import dagger.Binds
import dagger.android.AndroidInjector
import dagger.android.support.FragmentKey
import dagger.multibindings.IntoMap
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.processors.PublishProcessor
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.navigationIconResource
import org.jetbrains.anko.appcompat.v7.titleResource
import org.jetbrains.anko.appcompat.v7.toolbar
import org.jetbrains.anko.design.appBarLayout
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.design.floatingActionButton
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by ilben on 13.4.2017.
 */
class ChatRoomListFragment : ButterDaggerFragment(), ChannelSearchListener, ChatRoomListContract.View {

    @dagger.Subcomponent(modules = arrayOf())
    interface Subcomponent : AndroidInjector<ChatRoomListFragment> {
        @dagger.Subcomponent.Builder
        abstract class Builder : AndroidInjector.Builder<ChatRoomListFragment>()
    }

    @dagger.Module(subcomponents = arrayOf(Subcomponent::class))
    abstract class Module {
        @Binds
        @IntoMap
        @FragmentKey(ChatRoomListFragment::class)
        abstract fun bindInjectorFactory(builder: Subcomponent.Builder): AndroidInjector.Factory<out Fragment>
    }

    @dagger.Component(modules = arrayOf(Module::class))
    interface Component

    private val toolbar by bindView<Toolbar>(R.id.toolbar)
    private val fabAddRoom by bindView<FloatingActionButton>(R.id.fab)
    private val list by bindView<ChatRoomListView>(R.id.list)

    private val subscriptions = CompositeDisposable()

    @Inject
    lateinit var presenter: ChatRoomListFragmentPresenter

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return ChatRoomListUi.createView(AnkoContext.create(container!!.context))
    }

    override fun onStart() {
        super.onStart()

        presenter.takeView(this)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        subscriptions.add(fabAddRoom.clicks()
                .subscribe {
                    fragmentManager.beginTransaction()
                            .add(R.id.container, SearchFragment().also { it.setTargetFragment(this, 0) })
                            .addToBackStack(null)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                            .commit()
                })

        subscriptions.add(toolbar.navigationClicks().subscribe { fragmentManager.popBackStack() })
    }

    override fun onStop() {
        super.onStop()

        presenter.dropView(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()

        subscriptions.clear()
    }

    override fun onChannelPicked(channel: Channel) {
        presenter.addRoom(channel)
    }

    override fun setRooms(rooms: Array<Channel>) {
        list.setRooms(rooms)
    }
}

object ChatRoomListUi : AnkoComponent<Context> {
    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        coordinatorLayout {
            layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)
            background = context.getAttributeDrawable(ATTRS_WINDOW_BACKGROUND)

            appBarLayout(theme = R.style.ThemeOverlay_AppTheme_ActionBar) {
                toolbar {
                    id = R.id.toolbar
                    titleResource = R.string.title_chat_room_list
                    navigationIconResource = R.drawable.abc_ic_ab_back_material
                    //inflateMenu(R.menu.chat_room_list)
                }.lparams(matchParent, wrapContent) {
                    scrollFlags = 0
                }
            }.lparams(matchParent, wrapContent)

            chatRoomListView {
                id = R.id.list
                clipToPadding = false
                verticalPadding = dip(8)
            }.lparams(matchParent, matchParent) {
                behavior = AppBarLayout.ScrollingViewBehavior()
            }

            floatingActionButton {
                id = R.id.fab
                contentDescription = resources.getText(R.string.action_add_room)
                imageResource = R.drawable.ic_add_control_24dp
            }.lparams(wrapContent, wrapContent) {
                behavior = FloatingActionButton.Behavior()
                gravity = GravityCompat.END or Gravity.BOTTOM
                margin = dip(16)
            }
        }
    }
}

@Singleton
class ChatRoomListFragmentPresenter @Inject constructor(private val connectionManager: ConnectionManager) {

    private var view: ChatRoomListContract.View? = null
    private val subscriptions = CompositeDisposable()
    private val addRooms = PublishProcessor.create<Channel>()

    @Suppress("UNCHECKED_CAST")
    fun takeView(view: ChatRoomListContract.View) {
        this.view = view

        val authenticatedConnection = connectionManager.connection()
                .flatMap { (it as? ChatState.Connected)?.chatConnection?.authentications() ?: Flowable.just(Option.None) }

        subscriptions.add(Flowable.combineLatest(arrayOf(authenticatedConnection, addRooms), {
            it[0] as Option<Connection> to it[1] as Channel
        })
                .flatMap {
                    val conn = it.first
                    val channel = it.second
                    when (conn) {
                        Option.None -> Flowable.never()
                        is Option.Some -> conn.value.joinRoom(channel)
                                .toFlowable<Unit>()
                    }
                }
                .subscribe())

        subscriptions.add(connectionManager.connection()
                .flatMap { (it as? ChatState.Connected)?.chatConnection?.authentications() ?: Flowable.just(Option.None) }
                .flatMap {
                    when (it) {
                        Option.None -> Flowable.just(Option.None)
                        is Option.Some -> it.value.rooms()
                                .map { Option.Some(it) }
                    }
                }
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    val e = when (it) {
                        Option.None -> this.view?.setRooms(emptyArray())
                        is Option.Some -> this.view?.setRooms(it.value.map { it.channel }.toTypedArray())
                    }
                }
        )
    }

    fun dropView(view: ChatRoomListContract.View) {
        if (this.view === view) {
            subscriptions.clear()
            this.view = null
        }
    }

    fun addRoom(channel: Channel) {
        addRooms.onNext(channel)
    }
}