package com.ij.bttvoverlay.chat

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.IBinder
import android.support.v4.app.Fragment

/**
 * Starts and keeps the [ConnectionService] running.
 *
 * Created by ilben on 21.4.2017.
 */
class ConnectionFragment : Fragment() {

    private val serviceConnection = ServiceConnection()

    override fun onAttach(context: Context) {
        super.onAttach(context)

        retainInstance = true

        val intent = Intent(context, ConnectionService::class.java)

        //ensure that the service is alive
        context.startService(intent)

        //start the service which will keep the onLoaded alive
        context.bindService(intent, serviceConnection, 0)
    }

    override fun onStart() {
        super.onStart()

        serviceConnection.notifyAppOnForeground()
    }

    override fun onStop() {
        super.onStop()

        serviceConnection.notifyAppOnBackground()
    }

    override fun onDetach() {
        super.onDetach()

        context.unbindService(serviceConnection)
    }

    private class ServiceConnection : android.content.ServiceConnection {

        private var service: ConnectionService.ConnectionInterface? = null

        fun notifyAppOnBackground() {
            this.service?.onAppOnBackground()
        }

        fun notifyAppOnForeground() {
            this.service?.onAppOnForeground()
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            this.service = null
        }

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            if (service is ConnectionService.ConnectionInterface) {
                this.service = service
            }
        }

    }

}