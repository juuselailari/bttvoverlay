package com.ij.bttvoverlay

import io.reactivex.Flowable
import io.reactivex.processors.BehaviorProcessor
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by ilben on 6.4.2017.
 */
@Singleton
class AuthenticationManager @Inject constructor(){

    private val authSubject = BehaviorProcessor.createDefault<Option<Auth>>(Option.None)

    fun authentication(): Flowable<Option<Auth>> {
        return authSubject
    }

    fun authChanges(): Flowable<Option<Auth>> {
        return authSubject.skip(1)
    }

    fun setAuth(auth: Auth) {
        this.authSubject.onNext(Option.Some(auth))
    }

    fun clearAuth() {
        this.authSubject.onNext(Option.None)
    }

}

class Auth(val oauth: String, val username: String)