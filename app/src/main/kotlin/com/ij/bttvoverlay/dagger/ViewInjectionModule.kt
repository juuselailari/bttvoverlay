package com.ij.bttvoverlay.dagger

import android.view.View
import dagger.Module
import dagger.android.AndroidInjector
import dagger.multibindings.Multibinds

/**
 * Created by ilben on 2.5.2017.
 */
@Module
abstract class ViewInjectionModule {
    @Multibinds
    abstract fun viewInjectorFactories(): Map<Class<out View>, AndroidInjector.Factory<out View>>
}
