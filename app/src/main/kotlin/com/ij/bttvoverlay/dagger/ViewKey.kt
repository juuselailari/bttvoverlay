package com.ij.bttvoverlay.dagger

import android.view.View
import dagger.MapKey
import kotlin.reflect.KClass

/**
 * Created by ilben on 2.5.2017.
 */
@MapKey
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
annotation class ViewKey(val value: KClass<out View>)