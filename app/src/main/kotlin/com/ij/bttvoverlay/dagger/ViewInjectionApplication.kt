package com.ij.bttvoverlay.dagger

import android.view.View
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.DaggerApplication
import javax.inject.Inject

/**
 * Created by ilben on 2.5.2017.
 */
abstract class ViewInjectionApplication : DaggerApplication(), HasViewInjector {

    @Inject
    lateinit var viewInjector: DispatchingAndroidInjector<View>

    override fun getSystemService(name: String?): Any {
        if (ViewInjections.VIEW_INJECTION_SERVICE == name) return this
        return super.getSystemService(name)
    }

    override fun viewInjector(): AndroidInjector<View> {
        return viewInjector
    }
}