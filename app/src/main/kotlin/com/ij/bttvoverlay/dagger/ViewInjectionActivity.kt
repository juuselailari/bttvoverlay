package com.ij.bttvoverlay.dagger

import android.os.Bundle
import android.view.View
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

/**
 * Created by ilben on 2.5.2017.
 */
open class ViewInjectionActivity : DaggerAppCompatActivity(), HasViewInjector {

    @Inject
    lateinit var viewInjector: DispatchingAndroidInjector<View>

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun getSystemService(name: String?): Any {
        if (ViewInjections.VIEW_INJECTION_SERVICE == name) return this
        return super.getSystemService(name)
    }

    override fun viewInjector(): AndroidInjector<View> {
        return viewInjector
    }
}