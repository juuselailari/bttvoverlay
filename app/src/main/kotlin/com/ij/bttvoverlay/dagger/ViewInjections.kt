package com.ij.bttvoverlay.dagger

import android.view.View

/**
 * Created by ilben on 2.5.2017.
 */
object ViewInjections {

    const val VIEW_INJECTION_SERVICE = "com.ij.bttvoverlay.dagger.ViewInjectorService"

    fun inject(view: View) {
        val service = view.context.getSystemService(VIEW_INJECTION_SERVICE)
                ?: view.context.applicationContext.getSystemService(VIEW_INJECTION_SERVICE)
                ?: throw RuntimeException("ViewInjectorService missing")
        if (service is HasViewInjector) {
            val injector = service.viewInjector()
            injector.inject(view)
        } else {
            throw RuntimeException("${service.javaClass.canonicalName} doesn't implement ${HasViewInjector::class.java.simpleName}")
        }
    }
}


