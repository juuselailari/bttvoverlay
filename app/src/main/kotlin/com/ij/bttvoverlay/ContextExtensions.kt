package com.ij.bttvoverlay

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.annotation.ColorInt
import android.support.annotation.ColorRes
import android.support.v4.content.ContextCompat

val ATTRS_WINDOW_BACKGROUND = intArrayOf(android.R.attr.windowBackground)

val ATTRS_COLOR_PRIMARY = intArrayOf(R.attr.colorPrimary)
val ATTRS_COLOR_PRIMARY_DARK = intArrayOf(R.attr.colorPrimaryDark)
val ATTRS_COLOR_ACCENT = intArrayOf(R.attr.colorAccent)
val ATTRS_COLOR_CONTROL_HIGHLIGHT = intArrayOf(R.attr.colorControlHighlight)
val ATTRS_COLOR_CONTROL_NORMAL = intArrayOf(R.attr.colorControlNormal)
val ATTRS_TEXT_COLOR_PRIMARY = intArrayOf(android.R.attr.textColorPrimary)
val ATTRS_TEXT_COLOR_SECONDARY = intArrayOf(android.R.attr.textColorSecondary)

val ATTRS_TEXT_COLOR_PRIMARY_INVERSE = intArrayOf(android.R.attr.textColorPrimaryInverse)
val ATTRS_TEXT_COLOR_SECONDARY_INVERSE = intArrayOf(android.R.attr.textColorSecondaryInverse)

val ATTRS_DIVIDER_VERTICAL = intArrayOf(android.R.attr.listDivider)
val ATTRS_DIVIDER_HORIZONTAL = intArrayOf(android.R.attr.listDivider)
val ATTRS_SELECTABLE_ITEM_BACKGROUND = intArrayOf(R.attr.selectableItemBackground)
val ATTRS_SELECTABLE_ITEM_BACKGROUND_BORDERLESS = intArrayOf(R.attr.selectableItemBackgroundBorderless)

val ATTRS_DIALOG_PREFERRED_PADDING = intArrayOf(R.attr.dialogPreferredPadding)

val EMPTY_STATE_SET = IntArray(0)

fun Context.getAttributeDrawable(attrs: IntArray): Drawable {
    val a = obtainStyledAttributes(attrs)
    val result = a.getDrawable(0)
    a.recycle()
    return result
}

fun Context.getOptionalAttributeDrawable(attrs: IntArray, default: Drawable? = null): Drawable? {
    val a = obtainStyledAttributes(attrs)
    val result = a.getDrawable(0)
    a.recycle()
    return result ?: default
}

fun Context.getAttributeColor(attrs: IntArray, defValue: Int): Int {
    val a = obtainStyledAttributes(attrs)
    val result = a.getColor(0, defValue)
    a.recycle()
    return result
}

fun Context.getAttributeDimension(attrs: IntArray, defValue: Int): Int {
    val a = obtainStyledAttributes(attrs)
    val result = a.getDimensionPixelOffset(0, defValue)
    a.recycle()
    return result
}

@ColorInt
fun Context.getColorCompat(@ColorRes colorRes: Int): Int {
    return ContextCompat.getColor(this, colorRes)
}