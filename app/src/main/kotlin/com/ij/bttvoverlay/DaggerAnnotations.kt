package com.ij.bttvoverlay

import javax.inject.Qualifier

@Qualifier
annotation class AppContext

@Qualifier
annotation class DefaultPreferences

@Qualifier
annotation class BaseClient

@Qualifier
annotation class TwitchClient

@Qualifier
annotation class MainThreadScheduler

@Qualifier
annotation class IoScheduler