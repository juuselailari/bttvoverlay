package com.ij.bttvoverlay

import com.eaio.stringsearch.BNDMWildcards
import com.eaio.stringsearch.BoyerMooreHorspoolRaita
import com.ij.bttvoverlay.model.BadgeSets
import com.ij.bttvoverlay.model.Channel
import com.ij.bttvoverlay.model.ImageUrl
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by ilben on 6.4.2017.
 */
@Singleton
class EmoticonManager @Inject constructor() {

    companion object {
        private fun applyStats(emoticons: Array<Emoticon>, holder: EmoteStats) {
            emoticons.forEach {
                if (holder.shortest > it.pattern.size) {
                    holder.shortest = it.pattern.size
                }
                if (holder.longest < it.pattern.size) {
                    holder.longest = it.pattern.size
                }
            }
        }
    }

    private val stringSearch = BoyerMooreHorspoolRaita()
    private val globalEmoticons = ArrayList<Emoticon>()
    private val globalEmoteStats = EmoteStats(Int.MAX_VALUE, Int.MIN_VALUE)
    private var globalBadgeSets: BadgeSets? = null
    private val badgeSetsPerChannel = HashMap<Long, BadgeSets>()
    private val emotesPerChannel = HashMap<Long, ChannelEmotes>()

    private val wildCardChar = '.'
    private val wordSearch = BNDMWildcards(wildCardChar)
    private val wordEndPattern = charArrayOf(' ', wildCardChar)
    private val wordEndPatternProcessed = wordSearch.processChars(wordEndPattern)

    fun addGlobalEmotes(emoticons: Array<Emoticon>) {
        this.globalEmoticons.addAll(emoticons)
        applyStats(emoticons, globalEmoteStats)
    }

    fun setEmotes(channel: Channel, emoticons: Array<Emoticon>) {
        val stats = EmoteStats(Int.MAX_VALUE, Int.MIN_VALUE)
        applyStats(emoticons, stats)
        this.emotesPerChannel.put(channel.id, ChannelEmotes(emoticons, stats))
    }

    fun setGlobalBadgeSets(badgeSets: BadgeSets) {
        globalBadgeSets = badgeSets
    }

    fun getGlobalBadgeSets(): BadgeSets? = globalBadgeSets

    fun setBadgeSets(channel: Channel, badgeSets: BadgeSets) {
        badgeSetsPerChannel.put(channel.id, badgeSets)
    }

    fun getBadges(channelId: Long): BadgeSets? {
        return badgeSetsPerChannel[channelId]
    }

    fun applyEmotes(cs: String, channelId: Long?): List<EmoteSpan> {
        val result = HashMap<CharArray, EmoteSpanEntry>()
        val text = BoyerMooreHorspoolRaita.getChars(cs)

        val shortestEmote: Int
        val longestEmote: Int

        val emoticons: ArrayList<Emoticon>
        if (channelId != null) {
            val emotesPerChannel = emotesPerChannel[channelId]
            val channelEmotes: Array<Emoticon>
            if (emotesPerChannel != null) {
                shortestEmote = Math.min(globalEmoteStats.shortest, emotesPerChannel.stats.shortest)
                longestEmote = Math.max(globalEmoteStats.longest, emotesPerChannel.stats.longest)
                channelEmotes = emotesPerChannel.emotes
            } else {
                shortestEmote = globalEmoteStats.shortest
                longestEmote = globalEmoteStats.longest
                channelEmotes = emptyArray()
            }

            emoticons = ArrayList(channelEmotes.size + this.globalEmoticons.size)
            // Put the more specific emotes first
            emoticons.addAll(channelEmotes)
            emoticons.addAll(this.globalEmoticons)
        } else {
            shortestEmote = globalEmoteStats.shortest
            longestEmote = globalEmoteStats.longest
            emoticons = this.globalEmoticons
        }

        var i = if (!text[0].isWhitespace()) 0 else wordSearch.searchChars(text, 0, wordEndPattern, wordEndPatternProcessed) + 1
        loop@ while (i < text.size) {
            val wordStart = i
            val wordEnd = wordSearch.searchChars(text, i, wordEndPattern, wordEndPatternProcessed)
                    .let { if (it == -1) text.size else it }
            val wordLen = wordEnd - wordStart

            if (shortestEmote > wordLen) {
                val next = cs.indexOfNot(' ', wordStart + wordLen)
                if (next == -1) {
                    break
                } else {
                    i = next
                    continue
                }
            } else if (longestEmote < wordLen) {
                val next = cs.indexOfNot(' ', wordStart + wordLen)
                if (next == -1) {
                    break
                } else {
                    i = next
                    continue
                }
            }

            var found = false
            for (emote in emoticons) {
                if (emote.pattern.size != wordLen) {
                    continue
                }

                val searchStart = wordStart
                val searchEnd = searchStart + emote.pattern.size

                val start = stringSearch.searchChars(text, searchStart, searchEnd, emote.pattern, emote.processed)
                val end = start + emote.pattern.size

                if (start < 0) {
                    continue
                }

                val entry = result.getOrPut(emote.pattern, { EmoteSpanEntry(emote, arrayListOf(), arrayListOf()) })
                entry.starts.add(start)
                //ends are inclusive
                entry.ends.add(end - 1)

                val next = cs.indexOfNot(' ', wordStart + wordLen)
                if (next == -1) {
                    break@loop
                }

                i = next
                found = true
                break
            }

            if (!found) {
                val next = cs.indexOfNot(' ', wordStart + wordLen)
                if (next == -1) {
                    break@loop
                }

                i = next
            }
        }

        return result.values
                .toList()
                .map { EmoteSpan(it.emote, it.starts.toIntArray(), it.ends.toIntArray()) }
    }

    fun process(text: String): Any? {
        return stringSearch.processString(text)
    }

    /**
     * @return The first index where the character is not [char]
     */
    private fun CharSequence.indexOfNot(char: Char, startIndex: Int = 0): Int {
        val len = this.length
        if (startIndex == len) return -1

        var i = startIndex
        while (this[i] == char) {
            ++i
            if (i == len) {
                return -1
            }
        }
        return i
    }

}

private class ChannelEmotes(val emotes: Array<Emoticon>, val stats: EmoteStats)

private class EmoteStats(var shortest: Int, var longest: Int)

private class EmoteSpanEntry(val emote: Emoticon,
                             var starts: ArrayList<Int>,
                             /**Inclusive ends*/
                             var ends: ArrayList<Int>)

data class EmoteSpan(val emote: Emoticon,
                     val starts: IntArray,
                     val ends: IntArray)

data class Emoticon(val pattern: CharArray,
                    val processed: Any?,
                    val images: Array<Image>)

data class Image(val urls: Array<ImageUrl>)