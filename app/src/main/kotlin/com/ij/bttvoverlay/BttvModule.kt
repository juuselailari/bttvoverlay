package com.ij.bttvoverlay

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializer
import com.ij.bttvoverlay.api.BttvApi
import com.ij.bttvoverlay.api.TwitchApi
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import org.jetbrains.anko.defaultSharedPreferences
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by ilben on 2.5.2017.
 */
@Module
class BttvModule {

    @Provides
    @MainThreadScheduler
    fun provideMainThreadScheduler(): Scheduler = AndroidSchedulers.mainThread()

    @Provides
    @IoScheduler
    fun provideIoScheduler(): Scheduler = Schedulers.io()

    @Provides
    @AppContext
    fun provideAppContext(bttvApplication: BttvApplication): Context = bttvApplication.applicationContext

    @Provides
    @DefaultPreferences
    fun provideDefaultPreferences(@AppContext appContext: Context) = appContext.defaultSharedPreferences

    @Provides
    fun provideGson(): Gson = GsonBuilder()
            .registerTypeAdapter(Regex::class.java, JsonDeserializer<Regex> { json, _, _ -> Regex.fromLiteral(json.asString) })
            .registerTypeAdapter(HttpUrl::class.java, JsonDeserializer<HttpUrl> { json, _, _ -> HttpUrl.parse(json.asString) })
            .create()

    @Provides
    @BaseClient
    fun provideOkHttpClient(): OkHttpClient {
        return OkHttpClient()
    }

    @Provides
    @TwitchClient
    fun provideTwitchHttpClient(@BaseClient baseClient: OkHttpClient): OkHttpClient {
        return baseClient.newBuilder()
                .addInterceptor {
                    val req = it.request()
                            .newBuilder()
                            .addHeader("Client-ID", BuildConfig.TWITCH_API_KEY)
                            .build()
                    it.proceed(req)
                }
                .build()
    }

    @Provides
    fun provideTwitchApi(@TwitchClient twitchClient: OkHttpClient, gson: Gson): TwitchApi {
        return Retrofit.Builder()
                .baseUrl(HttpUrl.Builder()
                        .scheme("https")
                        .host("api.twitch.tv")
                        .build())
                .client(twitchClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(TwitchApi::class.java)
    }

    @Provides
    fun provideBttvApi(@BaseClient baseClient: OkHttpClient, gson: Gson): BttvApi {
        return Retrofit.Builder()
                .baseUrl(HttpUrl.Builder()
                        .scheme("https")
                        .host("api.betterttv.net")
                        .build())
                .client(baseClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(BttvApi::class.java)
    }

}