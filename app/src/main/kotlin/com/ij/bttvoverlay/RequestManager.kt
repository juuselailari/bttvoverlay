package com.ij.bttvoverlay

import com.ij.bttvoverlay.api.BttvApi
import io.reactivex.Observable

/**
 * Created by ilben on 25.4.2017.
 */
class RequestManager(val bttvApi: BttvApi) {

    fun load(what: String): Observable<Response> {
        return Observable.just<Response>(Response.Queued)
                .startWith(Response.InFlight)
                .flatMap {
                    bttvApi.getChannel(what)
                            .map<Response> { Response.Success(it.toString()) }
                            .onErrorReturn { Response.Error(it) }
                            .toObservable()
                }
    }

    sealed class Response {
        object Queued : Response()
        object InFlight : Response()
        data class Error(val throwable: Throwable) : Response()
        data class Success(val result: String) : Response()
    }

}