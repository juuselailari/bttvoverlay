package com.ij.bttvoverlay

import android.os.Parcel
import okhttp3.HttpUrl
import paperparcel.TypeAdapter

/**
 * Created by ilben on 21.4.2017.
 */
class HttpUrlTypeAdapter : TypeAdapter<HttpUrl> {

    override fun writeToParcel(httpUrl: HttpUrl, parcel: Parcel, flags: Int) = parcel.writeString(httpUrl.toString())

    override fun readFromParcel(parcel: Parcel): HttpUrl = HttpUrl.parse(parcel.toString())
}