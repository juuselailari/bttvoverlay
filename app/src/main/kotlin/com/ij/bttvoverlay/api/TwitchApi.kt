package com.ij.bttvoverlay.api

import com.google.gson.annotations.SerializedName
import com.ij.bttvoverlay.model.BadgeSets
import com.ij.bttvoverlay.model.ChannelBadges
import io.reactivex.Single
import retrofit2.http.*


/**
 * Created by ilben on 6.4.2017.
 */
interface TwitchApi {

    companion object {
        const val CHANNEL_BADGES_FORMAT = "//badges.twitch.tv/v1/badges/channels/%s/display"
    }

    @GET("kraken/users/{user}/emotes")
    fun loadEmoticons(@Path("user") username: String): Single<String>

    @GET("kraken/channel")
    fun loadChannel(@Header("Authorization") accessToken: String): Single<Channel>

    @GET("kraken/search/channels")
    fun findChannel(@Query("query") query: String): Single<ChannelsResponse>

    @GET("kraken/chat/{channel_id}/badges")
    @Headers("Accept: application/vnd.twitchtv.v5+json")
    fun loadBadges(@Path("channel_id") channelId: Long): Single<ChannelBadges>

    @GET
    fun loadGlobalBadgesBeta(@Url url: String = "//badges.twitch.tv/v1/badges/global/display"): Single<BadgeSets>

    @GET
    fun loadChannelBadgesBeta(@Url url: String): Single<BadgeSets>

}

data class Emoticon(val regex: Regex,
                    val images: Array<Image>)

data class Image(val width: Int,
                 val height: Int,
                 val url: String)

data class EmoticonsResponse(val emoticons: Array<Emoticon>)

data class Channel(
        @SerializedName("_id")
        val id: Long,
        val name: String,
        val logo: String)

data class ChannelsResponse(
        @SerializedName("_count")
        val count: String,
        val channels: Array<Channel>)