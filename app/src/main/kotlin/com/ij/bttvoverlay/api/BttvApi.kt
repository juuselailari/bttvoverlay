package com.ij.bttvoverlay.api

import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by Ilari Juusela on 29.1.2016.
 */
interface BttvApi {

    @GET("2/emotes")
    fun emotes(): Single<EmoteResponse>

    @GET("2/sets")
    fun sourceSets(): Single<EmoteSourceSetsResponse>

    @GET("2/channels/{channel}")
    fun getChannel(@Path("channel") channel: String): Single<ChannelResponse>

}

class ChannelResponse {

    var emotes: List<JsonEmote>? = null
    var urlTemplate: String? = null

}


class EmoteSourceSetsResponse {

    var status: String? = null
    var sets: List<String>? = null

}

data class EmoteResponse(val status: Int, val emotes: List<JsonEmote>, val urlTemplate: String)
data class JsonEmote(val id: String,
                     val code: String,
                     val channel: String?,
                     val imageType: String)

enum class Quality constructor(val value: String) {

    X1("1x") {
        override fun getScaledDimension(dimension: Int): Int {
            return dimension
        }
    },
    X2("2x") {
        override fun getScaledDimension(dimension: Int): Int {
            return dimension / 2
        }
    },
    X3("3x") {
        override fun getScaledDimension(dimension: Int): Int {
            return dimension / 4
        }
    };

    /**
     * Scales the given dimension to allow proper downscaling of image to the same size
     * as [X1].
     */
    abstract fun getScaledDimension(dimension: Int): Int
}
