package com.ij.bttvoverlay

/**
 * Created by ilben on 5.4.2017.
 */
sealed class Option<out T> {
    object None : Option<Nothing>() {

        override val isSome: Boolean = false

        override fun toString(): String {
            return "<None>"
        }
    }

    data class Some<out T>(val value: T) : Option<T>() {

        override val isSome: Boolean = true

        override fun toString(): String {
            return value.toString()
        }
    }

    abstract val isSome: Boolean

    companion object {

        fun <T> from(value: T?): Option<T> {
            return if (value == null) None else Option.Some(value)
        }

    }

    fun <R> map(callback: (T) -> R): Option<R> {
        return when (this) {
            Option.None -> Option.None
            is Option.Some -> Option.Some(callback(this.value))
        }
    }

}