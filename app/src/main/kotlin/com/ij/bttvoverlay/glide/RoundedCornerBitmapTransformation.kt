package com.ij.bttvoverlay.glide

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation


/**
 * Created by ilben on 20.4.2017.
 */
class RoundedCornerBitmapTransformation(private val context: Context,
                                        private val cornerRadius: Float = 0f,
                                        private val isCircular: Boolean = false) : BitmapTransformation(context) {

    override fun getId(): String = "rounded_corners_transform"

    override fun transform(pool: BitmapPool, toTransform: Bitmap, outWidth: Int, outHeight: Int): Bitmap {
        val output = pool.getDirty(toTransform.width, toTransform.height, toTransform.config)
                ?: Bitmap.createBitmap(toTransform.width, toTransform.height, Bitmap.Config.ARGB_8888)
        output.eraseColor(Color.TRANSPARENT)

        val canvas = Canvas(output)
        val drawable = RoundedBitmapDrawableFactory.create(context.resources, toTransform)

        if (isCircular) {
            drawable.isCircular = true
        } else {
            drawable.cornerRadius = cornerRadius
        }

        drawable.setBounds(0, 0, toTransform.width, toTransform.height)
        drawable.draw(canvas)

        return output
    }


}