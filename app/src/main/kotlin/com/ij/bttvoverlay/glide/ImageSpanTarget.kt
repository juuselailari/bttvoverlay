package com.ij.bttvoverlay.glide

import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.ImageSpan
import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.bumptech.glide.request.Request
import com.bumptech.glide.request.animation.GlideAnimation
import com.bumptech.glide.request.target.SizeReadyCallback
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.target.ViewTarget
import com.ij.bttvoverlay.chat.MessageView
import com.ij.bttvoverlay.chat.span.ImageLoadingSpan
import org.jetbrains.anko.displayMetrics


/**
 * A target that puts a downloaded image into an ImageSpan in the provided TextView.  It uses a
 * [ImageLoadingSpan] to mark the area to be replaced by the image.
 */
class ImageSpanTarget(textView: MessageView,
                      private val loadingSpans: List<ImageLoadingSpan>,
                      /**
                       * True if an image which supports animations (eg. GIF) should be animated.
                       */
                      private val animate: Boolean,
                      /**
                       * Scale to downscale the image.
                       */
                      private val imageScale: Float) : ViewTarget<MessageView, GlideDrawable>(textView) {

    private var resource: GlideDrawable? = null

    override fun setRequest(request: Request?) {

    }

    override fun getRequest(): Request? {
        return null
    }

    override fun onStart() {
        super.onStart()

        val resource = this.resource
        if (resource != null && resource.isAnimated && !resource.isRunning && animate) {
            resource.setLoopCount(GlideDrawable.LOOP_FOREVER)
            resource.start()
        }
    }

    override fun onStop() {
        super.onStop()

        this.view.clearManagedDrawables()
        this.resource?.stop()
    }

    override fun getSize(cb: SizeReadyCallback) {
        cb.onSizeReady(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
    }

    override fun onResourceReady(resource: GlideDrawable, glideAnimation: GlideAnimation<in GlideDrawable>) {
        this.resource = resource

        if (resource.isAnimated && !resource.isRunning && animate) {
            resource.setLoopCount(GlideDrawable.LOOP_FOREVER)
            resource.start()
            this.view.addManagedDrawable(resource)
        }

        val tv = view
        val bitmapDrawable = resource
        val scale = tv.context.displayMetrics.density / imageScale
        bitmapDrawable.setBounds(0, 0, (resource.minimumWidth * scale).toInt(), (resource.minimumHeight * scale).toInt())

        // add the image span and remove our marker
        val ssb = SpannableStringBuilder(tv.text)

        loadingSpans.forEach { loadingSpan ->
            val start = ssb.getSpanStart(loadingSpan)
            val end = ssb.getSpanEnd(loadingSpan)
            if (start >= 0 && end >= 0) {
                ssb.setSpan(ImageSpan(bitmapDrawable), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            }
            ssb.removeSpan(loadingSpan)
        }
        // animate the change
        tv.text = ssb
    }

}