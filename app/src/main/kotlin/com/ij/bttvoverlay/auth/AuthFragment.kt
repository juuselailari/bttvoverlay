package com.ij.bttvoverlay.auth

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar
import android.widget.Toast
import butterknife.bindView
import com.ij.bttvoverlay.ButterDaggerFragment
import com.ij.bttvoverlay.R
import dagger.Binds
import dagger.android.AndroidInjector
import dagger.android.support.FragmentKey
import dagger.multibindings.IntoMap
import okhttp3.HttpUrl
import org.jetbrains.anko.AnkoContext


/**
 * Created by ilben on 6.4.2017.
 */
class AuthFragment : ButterDaggerFragment() {

    @dagger.Subcomponent(modules = arrayOf())
    interface Subcomponent : AndroidInjector<AuthFragment> {
        @dagger.Subcomponent.Builder
        abstract class Builder : AndroidInjector.Builder<AuthFragment>()
    }

    @dagger.Module(subcomponents = arrayOf(Subcomponent::class))
    abstract class Module {
        @Binds
        @IntoMap
        @FragmentKey(AuthFragment::class)
        abstract fun bindInjectorFactory(builder: Subcomponent.Builder): AndroidInjector.Factory<out Fragment>
    }

    @dagger.Component(modules = arrayOf(Module::class))
    interface Component

    companion object {
        private val SCOPES = listOf("chat_login", "channel_read").joinToString(separator = " ")
    }

    private val progress by bindView<ProgressBar>(R.id.progress)
    private val webview by bindView<WebView>(R.id.webview)

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return AuthUI.createView(AnkoContext.create(container!!.context))
    }

    @Suppress("OverridingDeprecatedMember", "DEPRECATION")
    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        webview.settings.javaScriptEnabled = true
        webview.setWebChromeClient(object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, progress: Int) {
                // Activities and WebViews measure progress with different scales.
                // The progress meter will automatically disappear when we reach 100%
                this@AuthFragment.progress.setProgress(progress)
                if (progress == 100) {
                    this@AuthFragment.progress.visibility = View.GONE
                } else {
                    this@AuthFragment.progress.visibility = View.VISIBLE
                }
            }
        })
        webview.setWebViewClient(object : WebViewClient() {
            override fun onReceivedError(view: WebView, errorCode: Int, description: String, failingUrl: String) {
                Toast.makeText(activity, "Oh no! " + description, Toast.LENGTH_SHORT).show()
            }


            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                if (url.startsWith("http://localhost")) {
                    val fragment = HttpUrl.parse(url).fragment()
                    val values = fragment.split('&')
                            .map {
                                val cookie = it.split("=")
                                val key = cookie[0]
                                val value = cookie[1]
                                key to value
                            }.toMap()
                    val accessToken = values["access_token"]!!
                    fragmentManager.beginTransaction()
                            .add(R.id.container, FinishAuthFragment.create(accessToken))
                            .commit()
                    return true
                }
                return super.shouldOverrideUrlLoading(view, url)
            }
        })

        if (savedInstanceState == null) {
            webview.loadUrl("https://api.twitch.tv/kraken/oauth2/authorize?response_type=token&client_id=cgp78fatwubmyhu32ujayk8fuaqrar&redirect_uri=http://localhost&scope=$SCOPES")
        } else {
            webview.restoreState(savedInstanceState)
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        webview.saveState(outState)
    }

    override fun onDestroyView() {
        super.onDestroyView()

        webview.destroy()
    }

}