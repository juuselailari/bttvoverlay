package com.ij.bttvoverlay.auth

import android.content.Context
import android.view.View
import android.view.ViewGroup
import com.ij.bttvoverlay.R
import org.jetbrains.anko.*

/**
 * Created by ilben on 6.4.2017.
 */
object AuthUI : AnkoComponent<Context> {
    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        frameLayout {
            layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)
            webView {
                id = R.id.webview
            }.lparams(matchParent, matchParent)
            horizontalProgressBar {
                id = R.id.progress
            }.lparams(matchParent, wrapContent)
        }
    }
}