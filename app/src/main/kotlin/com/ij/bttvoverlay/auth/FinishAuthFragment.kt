package com.ij.bttvoverlay.auth

import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ij.bttvoverlay.*
import com.ij.bttvoverlay.api.TwitchApi
import com.ij.bttvoverlay.search.SearchFragment
import dagger.Binds
import dagger.android.AndroidInjector
import dagger.android.support.FragmentKey
import dagger.multibindings.IntoMap
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.support.v4.withArguments
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by ilben on 7.4.2017.
 */
class FinishAuthFragment : ButterDaggerFragment() {

    @dagger.Subcomponent(modules = arrayOf())
    interface Subcomponent : AndroidInjector<FinishAuthFragment> {
        @dagger.Subcomponent.Builder
        abstract class Builder : AndroidInjector.Builder<FinishAuthFragment>()
    }

    @dagger.Module(subcomponents = arrayOf(Subcomponent::class))
    abstract class Module {
        @Binds
        @IntoMap
        @FragmentKey(FinishAuthFragment::class)
        abstract fun bindInjectorFactory(builder: Subcomponent.Builder): AndroidInjector.Factory<out Fragment>
    }

    @dagger.Component(modules = arrayOf(Module::class))
    interface Component

    companion object {
        private const val ARG_ACCESS_TOKEN = "arg:access_token"

        fun create(accessToken: String): FinishAuthFragment {
            return FinishAuthFragment()
                    .withArguments(ARG_ACCESS_TOKEN to accessToken)
        }

    }

    @Inject
    lateinit var presenter: FinishAuthPresenter

    override fun onStart() {
        super.onStart()
        presenter.takeView(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (savedInstanceState == null) {
            presenter.setAccessToken(arguments.getString(ARG_ACCESS_TOKEN))
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return CoordinatorLayout(container!!.context)
                .apply {
                    layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)
                }
    }

    override fun onStop() {
        super.onStop()
        presenter.dropView(this)
    }

    fun finish() {
        fragmentManager.beginTransaction()
                .replace(R.id.container, SearchFragment())
                .commit()
    }

    fun showError(throwable: Throwable) {
        Snackbar.make(view!!, throwable.message!!, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.action_retry, { presenter.setAccessToken(arguments.getString(ARG_ACCESS_TOKEN)) })
                .show()
    }

}

@Singleton
class FinishAuthPresenter @Inject constructor(private val twitchApi: TwitchApi,
                                              private val authenticationManager: AuthenticationManager,
                                              @MainThreadScheduler private val mainThreadScheduler: Scheduler,
                                              @IoScheduler private val ioScheduler: Scheduler) {

    private var view: FinishAuthFragment? = null
    private val subscriptions = CompositeDisposable()

    private val accessToken = BehaviorSubject.createDefault<String>("")

    fun takeView(view: FinishAuthFragment) {
        this.view = view

        subscriptions.add(accessToken
                .filter { it.isNotEmpty() }
                .flatMap {
                    val token = it
                    twitchApi.loadChannel("OAuth $token")
                            .map<ChannelResult> { ChannelResult.Success(Auth(token, it.name)) }
                            .onErrorReturn { ChannelResult.Error(it) }
                            .toObservable()
                }
                .subscribeOn(ioScheduler)
                .observeOn(mainThreadScheduler)
                .subscribe({
                    val e = when (it) {
                        is ChannelResult.Success -> {
                            authenticationManager.setAuth(it.auth)
                            view.finish()
                        }
                        is ChannelResult.Error -> {
                            view.showError(it.throwable)
                        }
                    }
                }, {
                    throw it
                }))
    }

    fun setAccessToken(accessToken: String) {
        this.accessToken.onNext(accessToken)
    }

    fun dropView(view: FinishAuthFragment) {
        if (this.view === view) {
            this.view = null
        }
    }
}

sealed class ChannelResult {
    data class Success(val auth: Auth) : ChannelResult()
    data class Error(val throwable: Throwable) : ChannelResult()
}