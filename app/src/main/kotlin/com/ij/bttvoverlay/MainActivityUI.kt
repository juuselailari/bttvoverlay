package com.ij.bttvoverlay

import android.content.Context
import android.view.View
import android.view.ViewGroup
import org.jetbrains.anko.AnkoComponent
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.matchParent

/**
 * Created by ilben on 6.4.2017.
 */
object MainActivityUI : AnkoComponent<Context> {
    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        coordinatorLayout {
            id = R.id.container
            layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)
        }
    }


}