package com.ij.bttvoverlay.widget

import android.support.v7.widget.RecyclerView

/**
 * ItemAnimator which does not animations at all.
 *
 * Created by ilben on 27.4.2017.
 */
class EmptyItemAnimator : RecyclerView.ItemAnimator() {

    override fun isRunning(): Boolean = false

    override fun runPendingAnimations() = Unit

    override fun endAnimation(item: RecyclerView.ViewHolder?) = Unit

    override fun endAnimations() = Unit

    override fun animatePersistence(viewHolder: RecyclerView.ViewHolder, preLayoutInfo: ItemHolderInfo, postLayoutInfo: ItemHolderInfo): Boolean {
        dispatchAnimationFinished(viewHolder)
        return false
    }

    override fun animateDisappearance(viewHolder: RecyclerView.ViewHolder, preLayoutInfo: ItemHolderInfo, postLayoutInfo: ItemHolderInfo?): Boolean {
        dispatchAnimationFinished(viewHolder)
        return false
    }

    override fun animateChange(oldHolder: RecyclerView.ViewHolder, newHolder: RecyclerView.ViewHolder, preLayoutInfo: ItemHolderInfo, postLayoutInfo: ItemHolderInfo): Boolean {
        dispatchAnimationFinished(oldHolder)
        dispatchAnimationFinished(newHolder)
        return false
    }

    override fun animateAppearance(viewHolder: RecyclerView.ViewHolder, preLayoutInfo: ItemHolderInfo?, postLayoutInfo: ItemHolderInfo): Boolean {
        dispatchAnimationFinished(viewHolder)
        return false
    }


}