package com.ij.bttvoverlay.search

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.SearchView
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import butterknife.bindView
import com.ij.bttvoverlay.ButterDaggerFragment
import com.ij.bttvoverlay.R
import com.ij.bttvoverlay.chat.ChatFragment
import com.ij.bttvoverlay.model.Channel
import com.jakewharton.rxbinding2.support.v7.widget.queryTextChanges
import dagger.Binds
import dagger.android.AndroidInjector
import dagger.android.support.FragmentKey
import dagger.multibindings.IntoMap
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import org.jetbrains.anko.AnkoContext
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by ilben on 6.4.2017.
 */
class SearchFragment : ButterDaggerFragment(), SearchContract.View {

    @dagger.Subcomponent(modules = arrayOf())
    interface Subcomponent : AndroidInjector<SearchFragment> {
        @dagger.Subcomponent.Builder
        abstract class Builder : AndroidInjector.Builder<SearchFragment>()
    }

    @dagger.Module(subcomponents = arrayOf(Subcomponent::class))
    abstract class Module {
        @Binds
        @IntoMap
        @FragmentKey(SearchFragment::class)
        abstract fun bindInjectorFactory(builder: Subcomponent.Builder): AndroidInjector.Factory<out Fragment>
    }

    @dagger.Component(modules = arrayOf(Module::class))
    interface Component

    private val toolbar by bindView<Toolbar>(R.id.toolbar)
    private val list by bindView<ChannelListView>(R.id.list)
    private val progressBar by bindView<ProgressBar>(R.id.progress)

    private val subscriptions = CompositeDisposable()

    @Inject
    lateinit var presenter: SearchPresenter

    private var state: SearchContract.SearchState = SearchContract.SearchState.None()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return SearchFragmentUI.createView(AnkoContext.create(container!!.context))
    }

    @Suppress("IMPLICIT_CAST_TO_ANY", "UNCHECKED_CAST")
    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val searchView = MenuItemCompat.getActionView(toolbar.menu.findItem(R.id.app_bar_search)) as SearchView
        MenuItemCompat.expandActionView(toolbar.menu.findItem(R.id.app_bar_search))
        subscriptions.add(searchView.queryTextChanges()
                .debounce(300, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    presenter.searchChannels(it.toString())
                }))

        subscriptions.add(list.itemClicks()
                .subscribe { onChannelClick(it) })
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)

        savedInstanceState?.let { updateSearchState(it.getParcelable("state")) }
    }

    fun onChannelClick(channel: Channel) {
        val targetFragment = this.targetFragment
        if (targetFragment is ChannelSearchListener) {
            fragmentManager.popBackStack()
            targetFragment.onChannelPicked(channel)
        } else {
            fragmentManager.beginTransaction()
                    .replace(R.id.container, ChatFragment.create(channel), ChatFragment.TAG)
                    .commit()
        }
    }

    override fun onResume() {
        super.onResume()

        presenter.attach(this)
    }

    override fun onPause() {
        super.onPause()

        presenter.detach(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()

        subscriptions.clear()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        outState?.putParcelable("state", state)
    }

    override fun updateSearchState(state: SearchContract.SearchState) {
        val oldState = this.state
        this.state = state

        when (state) {
            is SearchContract.SearchState.Loading -> {
                if (oldState.query != state.query) {
                    progressBar.visibility = View.VISIBLE
                    list.clear()
                }
            }
            is SearchContract.SearchState.Finished -> {
                progressBar.visibility = View.GONE
                list.clear()
                list.addItems(0, state.messages)
            }
            is SearchContract.SearchState.Error -> {
                progressBar.visibility = View.GONE
                Snackbar.make(view!!, state.error.message!!, Snackbar.LENGTH_SHORT)
            }
        }
    }
}

interface ChannelSearchListener {

    fun onChannelPicked(channel: Channel)

}