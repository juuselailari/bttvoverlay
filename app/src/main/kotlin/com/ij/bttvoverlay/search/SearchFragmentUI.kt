package com.ij.bttvoverlay.search

import android.content.Context
import android.support.design.widget.AppBarLayout
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import com.ij.bttvoverlay.ATTRS_WINDOW_BACKGROUND
import com.ij.bttvoverlay.R
import com.ij.bttvoverlay.getAttributeDrawable
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.titleResource
import org.jetbrains.anko.appcompat.v7.toolbar
import org.jetbrains.anko.design.appBarLayout
import org.jetbrains.anko.design.coordinatorLayout

/**
 * Created by ilben on 6.4.2017.
 */
object SearchFragmentUI : AnkoComponent<Context> {
    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        coordinatorLayout {
            layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)
            background = context.getAttributeDrawable(ATTRS_WINDOW_BACKGROUND)

            appBarLayout(R.style.ThemeOverlay_AppTheme_ActionBar) {
                toolbar {
                    id = R.id.toolbar
                    titleResource = R.string.app_name
                    inflateMenu(R.menu.search)
                }.lparams(matchParent, wrapContent) {
                    scrollFlags = AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or
                            AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS
                }
            }.lparams(matchParent, wrapContent)

            channelListView {
                id = R.id.list
                verticalPadding = dip(8)
                clipToPadding = false
            }.lparams(matchParent, matchParent) {
                behavior = AppBarLayout.ScrollingViewBehavior()
            }

            progressBar {
                id = R.id.progress
                visibility = View.GONE
            }.lparams(wrapContent, wrapContent) {
                gravity = Gravity.CENTER
            }
        }
    }


}