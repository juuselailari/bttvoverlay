package com.ij.bttvoverlay.search

import android.os.Parcel
import android.os.Parcelable
import com.ij.bttvoverlay.model.Channel

/**
 * Created by ilben on 5.4.2017.
 */
interface SearchContract {
    interface View {

        fun updateSearchState(state: SearchState)

    }

    interface Presenter {

        fun attach(view: View)
        fun detach(view: View)

        fun searchChannels(query: String)

    }

    sealed class SearchState(val query: String) : Parcelable {
        class None() : SearchState("") {
            companion object {
                @JvmField val CREATOR = object : Parcelable.Creator<None> {
                    override fun newArray(size: Int): Array<None?> {
                        return arrayOfNulls(size)
                    }

                    override fun createFromParcel(source: Parcel): None {
                        return None()
                    }

                }
            }

            override fun writeToParcel(dest: Parcel, flags: Int) {
            }
        }

        class Loading(query: String) : SearchState(query) {
            companion object {
                @JvmField val CREATOR = object : Parcelable.Creator<Loading> {
                    override fun newArray(size: Int): Array<Loading?> {
                        return arrayOfNulls(size)
                    }

                    override fun createFromParcel(source: Parcel): Loading {
                        return Loading(source.readString())
                    }

                }
            }
        }

        @Suppress("UNCHECKED_CAST")
        class Finished(query: String, val messages: List<Channel>) : SearchState(query) {
            companion object {
                @JvmField val CREATOR = object : Parcelable.Creator<Finished> {
                    override fun newArray(size: Int): Array<Finished?> {
                        return arrayOfNulls(size)
                    }

                    override fun createFromParcel(source: Parcel): Finished {
                        return Finished(source.readString(), source.readArrayList(Channel::class.java.classLoader) as List<Channel>)
                    }

                }
            }

            override fun writeToParcel(dest: Parcel, flags: Int) {
                super.writeToParcel(dest, flags)
                dest.writeList(messages)
            }
        }

        class Error(query: String, val error: Throwable) : SearchState(query) {
            companion object {
                @JvmField val CREATOR = object : Parcelable.Creator<Error> {
                    override fun newArray(size: Int): Array<Error?> {
                        return arrayOfNulls(size)
                    }

                    override fun createFromParcel(source: Parcel): Error {
                        return Error(source.readString(), source.readSerializable() as Throwable)
                    }

                }
            }

            override fun writeToParcel(dest: Parcel, flags: Int) {
                super.writeToParcel(dest, flags)
                dest.writeSerializable(error)
            }
        }

        override fun writeToParcel(dest: Parcel, flags: Int) {
            dest.writeString(query)
        }

        override fun describeContents(): Int = 0
    }
}