package com.ij.bttvoverlay.search

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.ViewGroup
import android.view.ViewManager
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.ij.bttvoverlay.ATTRS_SELECTABLE_ITEM_BACKGROUND
import com.ij.bttvoverlay.R
import com.ij.bttvoverlay.getAttributeDrawable
import com.ij.bttvoverlay.glide.RoundedCornerBitmapTransformation
import com.ij.bttvoverlay.model.Channel
import com.ij.bttvoverlay.search.ChannelListView.Companion.ANKO_FACTORY
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.tintedTextView
import org.jetbrains.anko.custom.ankoView

/**
 * Created by ilben on 6.4.2017.
 */
class ChannelListView(context: Context) : RecyclerView(context) {

    companion object {
        val ANKO_FACTORY = { ctx: Context -> ChannelListView(ctx) }
    }

    private val channelListAdapter = ChannelListAdapter(context)

    init {
        layoutManager = LinearLayoutManager(context)
        adapter = channelListAdapter
    }

    fun clear() {
        channelListAdapter.clear()
    }

    fun addItems(startPos: Int, items: List<Channel>) {
        channelListAdapter.addItems(startPos, items)
    }

    fun itemClicks(): Observable<Channel> = channelListAdapter.itemClicks()

}

class ChannelListAdapter(val context: Context) : RecyclerView.Adapter<ChannelViewHolder>() {

    private val channels = ArrayList<Channel>()
    private val itemClicks = PublishSubject.create<Channel>()

    fun itemClicks(): Observable<Channel> = itemClicks

    fun clear() {
        val itemCount = this.channels.size
        this.channels.clear()
        notifyItemRangeRemoved(0, itemCount)
    }

    fun addItems(startPos: Int, items: List<Channel>) {
        this.channels.addAll(startPos, items)
        notifyItemRangeInserted(startPos, items.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChannelViewHolder {
        val view = ChannelView(context)
        view.onClick {
            itemClicks.onNext(view.getChannel())
        }
        return ChannelViewHolder(view)
    }

    override fun onBindViewHolder(holder: ChannelViewHolder, position: Int) {
        holder.view.bind(this.channels[position])
    }

    override fun getItemCount(): Int {
        return channels.size
    }


}

class ChannelViewHolder(val view: ChannelView) : RecyclerView.ViewHolder(view)

class ChannelView(context: Context) : FrameLayout(context) {

    private lateinit var image: ImageView
    private lateinit var text: TextView

    private var channel: Channel? = null

    init {
        layoutParams = ViewGroup.LayoutParams(matchParent, wrapContent)
        with(this) {
            foreground = context.getAttributeDrawable(ATTRS_SELECTABLE_ITEM_BACKGROUND)
            image = imageView {
                layoutParams = FrameLayout.LayoutParams(dip(48), dip(48)).apply {
                    margin = dip(16)
                }
            }
            text = tintedTextView {
                setTextAppearance(R.style.TextAppearance_AppCompat_Subhead)
                layoutParams = FrameLayout.LayoutParams(matchParent, matchParent).apply {
                    marginStart = dip(80)
                    marginEnd = dip(16)
                    gravity = Gravity.CENTER_VERTICAL
                }
            }
        }
    }

    fun bind(channel: Channel) {
        this.channel = channel
        Glide.with(context)
                .load(channel.icon)
                .transform(RoundedCornerBitmapTransformation(context, isCircular = true))
                .into(image)
        text.text = channel.name
    }

    fun getChannel() = channel

}


inline fun ViewManager.channelListView(theme: Int = 0, init: ChannelListView.() -> Unit): ChannelListView {
    return ankoView(ANKO_FACTORY, theme) { init() }
}