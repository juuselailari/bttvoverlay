package com.ij.bttvoverlay.search

import com.ij.bttvoverlay.IoScheduler
import com.ij.bttvoverlay.MainThreadScheduler
import com.ij.bttvoverlay.api.TwitchApi
import com.ij.bttvoverlay.model.Channel
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Function
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by ilben on 6.4.2017.
 */
@Singleton
class SearchPresenter @Inject constructor(val api: TwitchApi,
                                          @MainThreadScheduler private val mainThreadScheduler: Scheduler,
                                          @IoScheduler private val ioScheduler: Scheduler) : SearchContract.Presenter {

    private val searchQueries = PublishSubject.create<String>()
    private val subscriptions = CompositeDisposable()

    private var view: SearchContract.View? = null

    override fun attach(view: SearchContract.View) {
        this.view = view

        subscriptions.add(searchQueries
                .filter { it.length > 3 }
                .flatMap {
                    val query = it
                    api.findChannel(it)
                            .map<SearchContract.SearchState> { SearchContract.SearchState.Finished(query, it.channels.map { Channel(it.id, it.name, it.logo) }) }
                            .subscribeOn(ioScheduler)
                            .observeOn(mainThreadScheduler)
                            .toObservable()
                            .startWith(SearchContract.SearchState.Loading(query))
                            .onErrorResumeNext(Function { Observable.just(SearchContract.SearchState.Error(query, it)) })
                }
                .subscribe({
                    view.updateSearchState(it)
                }, {
                    throw it
                }))
    }

    override fun detach(view: SearchContract.View) {
        if (view === this.view) {
            subscriptions.clear()
            this.view = null
        }
    }

    override fun searchChannels(query: String) {
        searchQueries.onNext(query)
    }
}