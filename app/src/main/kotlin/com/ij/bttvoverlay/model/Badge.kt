package com.ij.bttvoverlay.model

import com.google.gson.annotations.SerializedName
import okhttp3.HttpUrl

/**
 * Created by ilben on 6.4.2017.
 */
class Badge(val image: HttpUrl)

typealias ChannelBadges = HashMap<String, Badge>

typealias BadgeSetName = String
data class BadgeSets(@SerializedName("badge_sets") val badgeSets: HashMap<BadgeSetName, BadgeSet>)
typealias BadgeVersion = String
data class BadgeSet(val versions: HashMap<BadgeVersion, BadgeBeta>)
data class BadgeBeta(val title: String,
                     val description: String,
                     @SerializedName("image_url_1x")
                     val imageUrl1x: HttpUrl,
                     @SerializedName("image_url_2x")
                     val imageUrl2x: HttpUrl,
                     @SerializedName("image_url_4x")
                     val imageUrl4x: HttpUrl)