package com.ij.bttvoverlay.model

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by ilben on 6.4.2017.
 */
data class Channel(val id: Long,
                   val name: String,
                   val icon: String?) : Parcelable {

    companion object {
        @JvmField
        val CREATOR = object : Parcelable.Creator<Channel> {
            override fun newArray(size: Int): Array<Channel?> {
                return arrayOfNulls(size)
            }

            override fun createFromParcel(source: Parcel): Channel {
                val id = source.readLong()
                val name = source.readString()
                val hasIcon = source.readByte() == 1.toByte()
                return if (hasIcon) Channel(id, name, source.readString()) else Channel(id, name, null)
            }

        }
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeLong(id)
        dest.writeString(name)
        dest.writeByte(if (icon == null) 0 else 1)
        if (icon != null) dest.writeString(icon)
    }

    override fun describeContents(): Int = 0

}