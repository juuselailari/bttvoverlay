package com.ij.bttvoverlay.model

import okhttp3.HttpUrl

/**
 * Created by ilben on 24.4.2017.
 */
data class ImageUrl(val url: HttpUrl, val scale: Float)