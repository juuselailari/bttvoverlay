package com.ij.bttvoverlay.model

import android.os.Parcel
import android.os.Parcelable
import okhttp3.HttpUrl
import paperparcel.PaperParcel
import java.util.*

/**
 * Created by ilben on 27.4.2017.
 */

sealed class Message : Parcelable {

    @PaperParcel
    class Invalid(val line: String) : Message(), Parcelable {
        companion object {
            @JvmField val CREATOR = PaperParcelMessage_Invalid.CREATOR
        }

        override fun describeContents() = 0
        override fun writeToParcel(dest: Parcel, flags: Int) = PaperParcelMessage_Invalid.writeToParcel(this, dest, flags)
    }

    @PaperParcel
    class PrivMsg(val received: Date,
                  val sent: Date?,
                  val user: User,
                  val roomName: String,
                  val message: String,
                  val isAction: Boolean,
                  val emotes: Array<Emote>,
                  val badges: Array<Badge>,
                  val mentions: Array<Mention>,
                  val images: Array<Image>,
                  val links: Array<Link>) : Message(), Parcelable {
        companion object {
            @JvmField val CREATOR = PaperParcelMessage_PrivMsg.CREATOR
        }

        override fun describeContents() = 0
        override fun writeToParcel(dest: Parcel, flags: Int) = PaperParcelMessage_PrivMsg.writeToParcel(this, dest, flags)
    }

    @PaperParcel
    class Connect() : Message(), Parcelable {
        companion object {
            @JvmField val CREATOR = PaperParcelMessage_Connect.CREATOR
        }

        override fun describeContents() = 0
        override fun writeToParcel(dest: Parcel, flags: Int) = PaperParcelMessage_Connect.writeToParcel(this, dest, flags)
    }

    @PaperParcel
    class Join(val name: String) : Message(), Parcelable {
        companion object {
            @JvmField val CREATOR = PaperParcelMessage_Join.CREATOR
        }

        override fun describeContents() = 0
        override fun writeToParcel(dest: Parcel, flags: Int) = PaperParcelMessage_Join.writeToParcel(this, dest, flags)
    }

    @PaperParcel
    class Part(val name: String) : Message(), Parcelable {
        companion object {
            @JvmField val CREATOR = PaperParcelMessage_Part.CREATOR
        }

        override fun describeContents() = 0
        override fun writeToParcel(dest: Parcel, flags: Int) = PaperParcelMessage_Part.writeToParcel(this, dest, flags)
    }

    @PaperParcel
    data class User(val name: String,
                    val host: Boolean,
                    val color: Int?) : Parcelable {
        companion object {
            @JvmField val CREATOR = PaperParcelMessage_User.CREATOR
        }

        override fun describeContents() = 0
        override fun writeToParcel(dest: Parcel, flags: Int) = PaperParcelMessage_User.writeToParcel(this, dest, flags)
    }

    @PaperParcel
    data class Emote(
            /** All versions of the emote url where the lowest quality one is first*/
            val urls: Array<ImageUrl>,
            val width: Int,
            val height: Int,
            val starts: IntArray,
            /**Inclusive end positions*/
            val ends: IntArray) : Parcelable {
        companion object {
            @JvmField val CREATOR = PaperParcelMessage_Emote.CREATOR
        }

        override fun describeContents() = 0
        override fun writeToParcel(dest: Parcel, flags: Int) = PaperParcelMessage_Emote.writeToParcel(this, dest, flags)
    }

    @PaperParcel
    data class Badge(
            /** All versions of the badge url where the lowest quality one is first*/
            val urls: Array<ImageUrl>,
            val title: String, val description: String) : Parcelable {
        companion object {
            @JvmField val CREATOR = PaperParcelMessage_Badge.CREATOR
        }

        override fun describeContents() = 0
        override fun writeToParcel(dest: Parcel, flags: Int) = PaperParcelMessage_Badge.writeToParcel(this, dest, flags)
    }

    @PaperParcel
    data class Mention(val host: Boolean,
                       val start: Int,
                       val end: Int) : Parcelable {
        companion object {
            @JvmField val CREATOR = PaperParcelMessage_Mention.CREATOR
        }

        override fun describeContents() = 0
        override fun writeToParcel(dest: Parcel, flags: Int) = PaperParcelMessage_Mention.writeToParcel(this, dest, flags)
    }

    @PaperParcel
    data class Image(val url: Url,
                     val start: Int,
                     val end: Int) : Parcelable {
        companion object {
            @JvmField val CREATOR = PaperParcelMessage_Image.CREATOR
        }

        override fun describeContents() = 0
        override fun writeToParcel(dest: Parcel, flags: Int) = PaperParcelMessage_Image.writeToParcel(this, dest, flags)
    }

    @PaperParcel
    data class ImageUrl(val url: HttpUrl,
                        val scale: Float) : Parcelable {
        companion object {
            @JvmField val CREATOR = PaperParcelMessage_ImageUrl.CREATOR
        }

        override fun describeContents() = 0
        override fun writeToParcel(dest: Parcel, flags: Int) = PaperParcelMessage_ImageUrl.writeToParcel(this, dest, flags)
    }

    @PaperParcel
    data class Link(val url: Url,
                    val start: Int,
                    val end: Int) : Parcelable {
        companion object {
            @JvmField val CREATOR = PaperParcelMessage_Link.CREATOR
        }

        override fun describeContents() = 0
        override fun writeToParcel(dest: Parcel, flags: Int) = PaperParcelMessage_Link.writeToParcel(this, dest, flags)
    }

    @PaperParcel
    data class Url(val url: HttpUrl,
                   val display: String,
                   val displayLengthChange: Int) : Parcelable {
        companion object {
            @JvmField val CREATOR = PaperParcelMessage_Url.CREATOR
        }

        override fun describeContents() = 0
        override fun writeToParcel(dest: Parcel, flags: Int) = PaperParcelMessage_Url.writeToParcel(this, dest, flags)
    }
}