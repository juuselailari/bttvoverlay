package com.ij.bttvoverlay

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import com.ij.bttvoverlay.auth.AuthFragment
import com.ij.bttvoverlay.chat.ChatFragment
import com.ij.bttvoverlay.chat.ConnectionService
import com.ij.bttvoverlay.dagger.ViewInjectionActivity
import dagger.Binds
import dagger.Component
import dagger.Module
import dagger.Subcomponent
import dagger.android.ActivityKey
import dagger.android.AndroidInjector
import dagger.android.support.DaggerAppCompatActivity
import dagger.multibindings.IntoMap
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.defaultSharedPreferences


/**
 * Created by ilben on 5.4.2017.
 */
class MainActivity : DaggerAppCompatActivity() {

    companion object {
        private const val ACTION_OPEN_CHAT = "com.ij.bttvoverlay.ACTION_OPEN_CHAT"

        fun createOpenChatIntent(context: Context): Intent {
            val chatIntent = Intent(context, MainActivity::class.java)
            chatIntent.action = ACTION_OPEN_CHAT
            return chatIntent
        }

    }

    private val subscriptions = CompositeDisposable()

    @Suppress("UNCHECKED_CAST")
    override fun onCreate(savedInstanceState: Bundle?) {
        val prefs = defaultSharedPreferences
        val theme = prefs.getString(getString(R.string.pref_theme), "dark")

        when (theme) {
            "dark" -> setTheme(R.style.AppTheme)
            "light" -> setTheme(R.style.AppTheme_Light)
            else -> Log.d("MainActivity", "Unknown theme $theme")
        }

        super.onCreate(savedInstanceState)

        setContentView(MainActivityUI.createView(AnkoContext.create(this, this)))

        if (!handleIntent(intent)) {
            if (savedInstanceState == null) {
                supportFragmentManager.beginTransaction()
                        .replace(R.id.container, AuthFragment())
                        .commit()
            }
        }

        // Listen for theme changes
        val themeKey = getString(R.string.pref_theme)
        subscriptions.add(Observable.create<String> { emitter ->
            val preferences = defaultSharedPreferences
            val callback = SharedPreferences.OnSharedPreferenceChangeListener { sp, key -> if (key == themeKey) emitter.onNext(sp.getString(key, "dark")) }
            preferences.registerOnSharedPreferenceChangeListener(callback)
            emitter.setCancellable { preferences.unregisterOnSharedPreferenceChangeListener(callback) }
        }.subscribe { recreate() })
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        if (intent != null) {
            handleIntent(intent)
        }
    }

    private fun handleIntent(intent: Intent): Boolean {
        return when (intent.action) {
            ACTION_OPEN_CHAT -> {
                val fm = supportFragmentManager
                val chatFragment = fm.findFragmentByTag(ChatFragment.TAG)
                if (chatFragment != null && !chatFragment.isVisible) {
                    fm.beginTransaction()
                            .replace(R.id.container, chatFragment)
                            .commit()
                }

                true
            }
            else -> false
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        subscriptions.clear()

        if (isFinishing) {
            stopService(Intent(this, ConnectionService::class.java))
        }
    }
}

@Subcomponent(modules = arrayOf())
interface MainActivitySubcomponent : AndroidInjector<MainActivity> {
    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<MainActivity>()
}

@Module(subcomponents = arrayOf(MainActivitySubcomponent::class))
abstract class MainActivityModule {
    @Binds
    @IntoMap
    @ActivityKey(MainActivity::class)
    abstract fun bindMainActivityInjectorFactory(builder: MainActivitySubcomponent.Builder): AndroidInjector.Factory<out Activity>
}

@Component(modules = arrayOf(MainActivityModule::class))
interface MainApplicationComponent