package com.ij.bttvoverlay

import android.support.v7.preference.PreferenceManager
import com.ij.bttvoverlay.auth.AuthFragment
import com.ij.bttvoverlay.auth.FinishAuthFragment
import com.ij.bttvoverlay.chat.ChatFragment
import com.ij.bttvoverlay.chat.ConnectionService
import com.ij.bttvoverlay.chat.rooms.ChatRoomListFragment
import com.ij.bttvoverlay.chat.rooms.RoomView
import com.ij.bttvoverlay.dagger.ViewInjectionApplication
import com.ij.bttvoverlay.dagger.ViewInjectionModule
import com.ij.bttvoverlay.search.SearchFragment
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import dagger.android.support.DaggerApplication
import paperparcel.Adapter
import paperparcel.ProcessorConfig
import javax.inject.Singleton


/**
 * Created by ilben on 6.4.2017.
 */
@ProcessorConfig(adapters = arrayOf(Adapter(HttpUrlTypeAdapter::class, nullSafe = false)))
class BttvApplication : ViewInjectionApplication() {

    @dagger.Component(modules = arrayOf(AndroidSupportInjectionModule::class,
            ViewInjectionModule::class,
            BttvModule::class,
            MainActivityModule::class,
            AuthFragment.Module::class,
            FinishAuthFragment.Module::class,
            ChatFragment.Module::class,
            ChatRoomListFragment.Module::class,
            SearchFragment.Module::class,
            ConnectionService.Module::class,
            RoomView.Module::class))
    @Singleton
    interface Component : AndroidInjector<BttvApplication> {
        @dagger.Component.Builder
        abstract class Builder : AndroidInjector.Builder<BttvApplication>()
    }

    override fun onCreate() {
        super.onCreate()

        PreferenceManager.setDefaultValues(this, R.xml.settings, false)
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerBttvApplication_Component.builder().create(this)
    }
}