package com.ij.bttvoverlay

import android.content.Context
import android.support.v4.app.Fragment
import butterknife.ButterFragment
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

/**
 * Created by ilben on 2.5.2017.
 */
open class ButterDaggerFragment : ButterFragment(), HasSupportFragmentInjector {
    @Inject
    lateinit var childFragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)

    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return this.childFragmentInjector
    }
}
