package com.ij.bttvoverlay

import java.util.regex.Pattern

/**
 * Created by ilben on 6.4.2017.
 */
object UrlUtils {

    private val mUrlTemplatePattern = Pattern.compile("\\{\\{[^}]*\\}\\}")

    fun applyToUrlTemplate(urlTemplate: String, vararg params: String): String {
        val result = StringBuilder()

        val matcher = mUrlTemplatePattern.matcher(urlTemplate)
        var count = 0
        var pos = 0
        while (matcher.find()) {
            val start = matcher.start()
            val end = matcher.end()

            result.append(urlTemplate, pos, start)
            pos += start - pos + (end - start)
            result.append(params[count])
            count++
        }

        return result.toString()
    }

}
