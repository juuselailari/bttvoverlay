# BttvOverlay
Android application for reading Twitch chat written in Kotlin.

# Notice
Application needs work on authenticating to Twitch and socket management
and in current state these may cause crashes and other issues.

# Setup
Kotlin plugin for gradle is required.

Add following variables to `bttvoverlay.properties`.

```
twitchApiKey=<TWITCH_API_KEY>
keyStoreFile=<PATH_TO_KEYSTORE>
keyStorePassword=<KEYSTORE_PASSWORD>
keyStoreKeyAlias=<KEYSTORE_KEY_ALIAS>
keyStoreKeyPassword=<KEYSTORE_KEY_PASSWORD>
```

# Features

![alt text](https://gitlab.com/juuselailari/bttvoverlay/raw/master/media/chat-light.png)
![alt text](https://gitlab.com/juuselailari/bttvoverlay/raw/master/media/chat-dark.png)

### Chat
In the current state the chat experience should be that of the official
Twitch Android application as the same features and more are implemented.

### Multiple chat rooms
After picking the initial chat room one can include other rooms from
the "chat room list" screen. Currently the messages 
from all of the chat rooms are combined to the same screen.

### Emotes
Chat supports the Twitch emotes and BetterTV emotes. It also uses
the twitch's experimental API for loading subscriber, admin and other badges.

### Mentions
Mentions are highlighted in the chat.

### Links
Links are highlighted in the chat and are clickable. The links are opened
in a application themed window with custom-tabs.

### Themes
Currently "light" and "dark" themes are included. Theme can be changed
from the applications "settings" screen.

# Preferences
Through preferences the user may change the core features of the chat screen
such as disable emotes and change image quality of the emotes.

![alt text](https://gitlab.com/juuselailari/bttvoverlay/raw/master/media/preferences.png)